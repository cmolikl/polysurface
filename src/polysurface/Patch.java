/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface;

import scene.surface.mesh.Mesh;

/**
 *
 * @author cmolikl
 */
public interface Patch {
    public void teselate(Mesh mesh, int res, boolean smoothShading);
    public void getPoints(Mesh mesh);
    public void getFaces(Mesh mesh);
}
