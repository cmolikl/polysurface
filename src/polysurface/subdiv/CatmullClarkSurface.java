/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface.subdiv;

import java.util.ArrayList;
import java.util.HashMap;
import javax.vecmath.Point4d;
import javax.vecmath.Vector3d;

import polysurface.Example;
import polysurface.PatchRenderer;
import polysurface.bezier.C2;
import scene.surface.Appearance;
import scene.surface.mesh.Face;
import scene.surface.mesh.Mesh;
import scene.Scene;
import scene.surface.mesh.Vertex;
import mesh.loaders.ObjLoader;
import scene.surface.mesh.Edge; 
import scene.surface.mesh.Edge.HalfEdge;
import scene.surface.mesh.MeshGeometry;

/**
 *
 * @author cmolikl
 */
public class CatmullClarkSurface extends SubdivisionSurface {

    HashMap<Face, Vertex> facePoints;
    HashMap<Edge, Vertex> edgePoints;
    HashMap<Vertex, Vertex> vertexPoints;

    public CatmullClarkSurface(Mesh controlMesh) {
        geometry = controlMesh.geometry;
        appearance = controlMesh.appearance;
    }

    public CatmullClarkSurface getDeepCopy() {
        Mesh mesh = super.getDeepCopy();
        mesh.geometry.determineEdges();
        return new CatmullClarkSurface(mesh);
    }

    private void subdivide(Mesh mesh) {
        facePoints = new HashMap<Face, Vertex>();
        edgePoints = new HashMap<Edge, Vertex>();
        vertexPoints = new HashMap<Vertex, Vertex>();
        
        // calculate face points
        for(Face f : mesh.getGeometry().getFaceList()) {
            Point4d facePoint = new Point4d(0.0, 0.0, 0.0, 0.0);
            Vector3d faceNormal = new Vector3d(0.0, 0.0, 0.0);
            for(HalfEdge edge : f) {
                facePoint.add(edge.v0().getLocation());
                Vector3d n = edge.v0().getNormal();
                n.normalize();
                faceNormal.add(n);
                edge = edge.next();
            }
            facePoint.scale(1f/facePoint.w);
            faceNormal.normalize();
            facePoints.put(f, new Vertex(facePoint, faceNormal));
        }
        
        // calculate edge mid points
        for(Edge edge : mesh.getGeometry().getEdgeList()) {
            Point4d edgePoint = new Point4d(0.0, 0.0, 0.0, 0.0);
            Vector3d edgeNormal = new Vector3d(0.0, 0.0, 0.0);
            HalfEdge he = edge.getHalfEdge(Edge.DIR);
            
            edgePoint.add(he.v0().getLocation());
            edgePoint.add(he.v1().getLocation());
            if(edge.f[0] != null) {
                edgePoint.add(facePoints.get(edge.f[0]).getLocation());
            }
            if(edge.f[1] != null) {
                edgePoint.add(facePoints.get(edge.f[1]).getLocation());
            }
            edgePoint.scale(1f/edgePoint.w);
            
            Vector3d n = he.v0().getNormal();
            n.normalize();
            edgeNormal.add(n);
            n = he.v1().getNormal();
            n.normalize();
            edgeNormal.add(n);
            if(edge.f[0] != null) {
                n = facePoints.get(edge.f[0]).getNormal();
                n.normalize();
                edgeNormal.add(n);
            }
            if(edge.f[1] != null) {
                n = facePoints.get(edge.f[1]).getNormal();
                n.normalize();
                edgeNormal.add(n);
            }
            edgeNormal.normalize();
            
            edgePoints.put(edge, new Vertex(edgePoint, edgeNormal));
        }
        
        // move vertices
        for(Vertex v : mesh.getGeometry().getVertexList()) {
            float n = 0f;
            Point4d s = new Point4d(0.0, 0.0, 0.0, 0.0);
            Vector3d sn = new Vector3d(0.0, 0.0, 0.0);
            Point4d r = new Point4d(0.0, 0.0, 0.0, 0.0);
            Point4d q = new Point4d(0.0, 0.0, 0.0, 0.0);
            for(HalfEdge edge : v) {
                r.add(edge.v1().getLocation());
                q.add(facePoints.get(edge.face()).getLocation());
                sn.add(facePoints.get(edge.face()).getNormal());
                n++;
            }
            
            r.scale(1f/r.w);
            q.scale(1f/q.w);
            
            s.add(v.getLocation());
            s.scale(n - 2); 
            s.add(r);
            s.add(q);
            s.scale(1f/s.w);
            
            sn.normalize();
            
            vertexPoints.put(v, new Vertex(s, sn));
        }
    }
    
    public void moveVertices(Mesh mesh) {    
        for(Vertex v : mesh.getGeometry().getVertexList()) {
            Vertex s = vertexPoints.get(v);
            v.setLocation(s.getLocation());
            v.setNormal(s.getNormal().x, s.getNormal().y, s.getNormal().z);
        }
    }
    
    public Mesh getSubdividedMesh(MeshGeometry mesh) { 
        
        MeshGeometry subdividedMesh = new MeshGeometry();
        
        // reconnect edge points
        ArrayList<Edge> edgesToAdd = new ArrayList<>();
        for(Edge edge : mesh.getEdgeList()) {
            Vertex edgePoint = edgePoints.get(edge);
            mesh.add(edgePoint);
            
            HalfEdge he = edge.getHalfEdge(Edge.DIR);
            HalfEdge twin = he.twin();
            
            Edge e = new Edge();
            HalfEdge firstHalf = e.createHalfEdge(he.v0(), edgePoint, Edge.DIR);
            HalfEdge firstTwin = e.createHalfEdge(edgePoint, twin.v1(), Edge.INV);
            
            if(he.v0().getEdge() == null) {
                he.v0().setEdge(firstHalf);
            }
            
            if(edgePoint.getEdge() == null) {
                edgePoint.setEdge(firstTwin);
            }
            
            if(he.v0().getEdge() == he) {
                he.v0().setEdge(firstHalf);
            }
            
            he.setV0(edgePoint);
            twin.setV1(edgePoint);
            
            firstHalf.setPrev(he.prev());
            he.prev().setNext(firstHalf);
            he.setPrev(firstHalf);
            
            firstTwin.setNext(twin.next());
            twin.next().setPrev(firstTwin);
            twin.setNext(firstTwin);
            
            firstHalf.setNext(he);
            firstTwin.setPrev(twin);
            
            firstHalf.setTwin(firstTwin);
            firstTwin.setTwin(firstHalf);
            
            firstHalf.setFace(he.face());
            firstTwin.setFace(twin.face());
            
            edgesToAdd.add(e);
        }
        mesh.getEdgeList().addAll(edgesToAdd);
        
        // reconect face points and create new faces
        ArrayList<Face> facesToAdd = new ArrayList<Face>();
        for(Face face : mesh.getFaceList()) {
            ArrayList<Vertex> faceEdgePoints = new ArrayList<Vertex>();
            for(HalfEdge he : face) {
                Vertex edgePoint = edgePoints.get(he.edge());
                if(edgePoint != null) {
                    faceEdgePoints.add(edgePoint);
                }
            }
            //System.out.println("Number of face edge points: " + faceEdgePoints.size());
            
            Vertex facePoint = facePoints.get(face);                
            Edge e = mesh.makeVertexEdge(faceEdgePoints.get(0), facePoint, face);
            mesh.addEdge(e);
            facePoint.setEdge(e.getHalfEdge(Edge.INV));
            mesh.add(facePoint);
            for(int i = 1; i < faceEdgePoints.size(); i++) {
                e = mesh.makeVertexEdge(faceEdgePoints.get(i), facePoint, face);
                mesh.addEdge(e);
                Face f = mesh.makeEdgeFace(e.getHalfEdge(Edge.DIR), new Face());
                facesToAdd.add(f);
            }
            face.setEdge(e.getHalfEdge(Edge.INV));
        }
        mesh.getFaceList().addAll(facesToAdd);
        
        return null;
    }
    
    public void reconnect(Mesh mesh) { 
        
        // reconnect edge points
        ArrayList<Edge> edgesToAdd = new ArrayList<>();
        for(Edge edge : mesh.getGeometry().getEdgeList()) {
            Vertex edgePoint = edgePoints.get(edge);
            mesh.geometry.add(edgePoint);
            
            HalfEdge he = edge.getHalfEdge(Edge.DIR);
            HalfEdge twin = he.twin();
            
            Edge e = new Edge();
            HalfEdge firstHalf = e.createHalfEdge(he.v0(), edgePoint, Edge.DIR);
            HalfEdge firstTwin = e.createHalfEdge(edgePoint, twin.v1(), Edge.INV);

            if(he.v0().getEdge() == null) {
                he.v0().setEdge(firstHalf);
            }

            if(edgePoint.getEdge() == null) {
                edgePoint.setEdge(firstTwin);
            }

            if(he.v0().getEdge() == he) {
                he.v0().setEdge(firstHalf);
            }

            he.setV0(edgePoint);
            twin.setV1(edgePoint);

            firstHalf.setPrev(he.prev());
            he.prev().setNext(firstHalf);
            he.setPrev(firstHalf);
            
            firstTwin.setNext(twin.next());
            twin.next().setPrev(firstTwin);
            twin.setNext(firstTwin);
            
            firstHalf.setNext(he);
            firstTwin.setPrev(twin);
            
            firstHalf.setTwin(firstTwin);
            firstTwin.setTwin(firstHalf);
            
            firstHalf.setFace(he.face());
            firstTwin.setFace(twin.face());
            
            edgesToAdd.add(e);
        }
        mesh.getGeometry().getEdgeList().addAll(edgesToAdd);
        
        // reconnect face points and create new faces
        ArrayList<Face> facesToAdd = new ArrayList<>();
        for(Face face : mesh.getGeometry().getFaceList()) {
            ArrayList<Vertex> faceEdgePoints = new ArrayList<>();
            for(HalfEdge he : face) {
                Vertex edgePoint = edgePoints.get(he.edge());
                if(edgePoint != null) {
                    faceEdgePoints.add(edgePoint);
                }
            }
            //System.out.println("Number of face edge points: " + faceEdgePoints.size());
            
            Vertex facePoint = facePoints.get(face);                
            Edge e = mesh.getGeometry().makeVertexEdge(faceEdgePoints.get(0), facePoint, face);
            mesh.getGeometry().addEdge(e);
            facePoint.setEdge(e.getHalfEdge(Edge.INV));
            mesh.geometry.add(facePoint);
            for(int i = 1; i < faceEdgePoints.size(); i++) {
                e = mesh.getGeometry().makeVertexEdge(faceEdgePoints.get(i), facePoint, face);
                mesh.getGeometry().addEdge(e);
                Face f = mesh.getGeometry().makeEdgeFace(e.getHalfEdge(Edge.DIR), new Face());
                facesToAdd.add(f);
            }
            face.setEdge(e.getHalfEdge(Edge.INV));
        }
        mesh.getGeometry().getFaceList().addAll(facesToAdd);
    }

    @Override
    public void teselate(Mesh mesh, int res, boolean smoothShading) {
        for(int i = 0; i < res; i++) {
            subdivide(mesh);
            moveVertices(mesh);
            reconnect(mesh);
        }
    }

    @Override
    public void getPoints(Mesh mesh) {

    }

    @Override
    public void getFaces(Mesh mesh) {

    }
}
