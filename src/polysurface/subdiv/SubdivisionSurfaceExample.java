/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface.subdiv;

import gleem.BSphere;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.media.opengl.GL;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import mesh.loaders.ObjLoader;
import polysurface.Example;
import polysurface.ExamplePanel;
import polysurface.Patch;
import polysurface.PatchRenderer;
import polysurface.bezier.C0;
import scene.surface.Appearance;
import scene.surface.mesh.Mesh;
import scene.surface.mesh.MeshUtils;
import scene.Scene;
import tiger.core.Effect;
import tiger.core.Pass;
import tiger.core.RenderState;
import tiger.core.Window;
import ui.control.BooleanCheckBox;
import ui.control.IntSlider;
import ui.control.ParameterLabel;
import ui.parameter.IntParameter;

/**
 *
 * @author cmolikl
 */
public class SubdivisionSurfaceExample extends Example {

    WingedEdgeMeshRenderer edgeRenderer = new WingedEdgeMeshRenderer();
    WingedEdgePointRenderer pointRenderer = new WingedEdgePointRenderer();

    IntParameter subdivisions = new IntParameter("Number of subdivisions", 1);
    IntParameter smoothShading = new IntParameter("Smooth shading", 1);

    Scene pointsScene;
    Scene edgesScene;
    Scene scene;

    private CatmullClarkSurface controlGrid;
    private CatmullClarkSurface edges;
    private CatmullClarkSurface points;

    IntParameter showControlGridParam = new IntParameter("Show control grid", 1);
    IntParameter showEdgesParam = new IntParameter("Show surface edges", 0);
    IntParameter showVerticesParam = new IntParameter("Show surface vertices", 0);

    @Override
    public Patch[] getPatches() {
        if(patches.length == 0) {
            ObjLoader loader = new ObjLoader();
            //Scene<Mesh> scene = loader.loadFile("C:/Users/cmolikl/Projects/Data/cube_quads.obj");
            InputStream stream = ClassLoader.getSystemResourceAsStream("polysurface/assets/suzanne.obj");
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            try {
                Scene<Mesh> scene = loader.loadFile(reader, null);


                Mesh mesh = scene.getMesh("Suzanne");
                mesh.geometry.determineEdges();

                CatmullClarkSurface ccs = new CatmullClarkSurface(mesh);

                patches = new Patch[]{ccs};
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return patches;
    }

    public void subdivide() {
        CatmullClarkSurface controlMesh = (CatmullClarkSurface) getPatches()[0];
        CatmullClarkSurface patch = controlMesh.getDeepCopy();
        patch.smoothShading = smoothShading.getValue() >= 1;

        patch.teselate(patch, subdivisions.getValue(), true);

        points = new CatmullClarkSurface(patch);
        points.facePoints = patch.facePoints;
        points.edgePoints = patch.edgePoints;
        points.vertexPoints = patch.vertexPoints;
        points.setRenderer(pointRenderer);
        points.setVisibility(showVerticesParam.getValue() == 1);

        if (pointsScene == null) {
            pointsScene = new Scene();
        }
        else {
            pointsScene.clear();
        }
        pointsScene.addMesh(points);

        controlGrid = new CatmullClarkSurface(controlMesh);
        controlGrid.setRenderer(edgeRenderer);
        controlGrid.appearance = new Appearance("ControlGrid");
        controlGrid.appearance.setDiffuse(0f, 0f, 1f);
        controlGrid.setVisibility(showControlGridParam.getValue() == 1);

        edges = new CatmullClarkSurface(patch);
        edges.setRenderer(edgeRenderer);
        edges.appearance = new Appearance("Edges");
        edges.appearance.setDiffuse(0.5f, 0.5f, 0.5f);
        edges.setVisibility(showEdgesParam.getValue() == 1);

        if(edgesScene == null) {
            edgesScene = new Scene();
        }
        else {
            edgesScene.clear();
        }
        edgesScene.addMesh(edges);
        edgesScene.addMesh(controlGrid);

        if(scene == null) {
            scene = new Scene();
        }
        else {
            scene.clear();
        }
        scene.addMesh(patch);

        BSphere bsphere = MeshUtils.getBoundingSphere(points);
        pointsScene.setBoundingSphere(bsphere);
        edgesScene.setBoundingSphere(bsphere);
        scene.setBoundingSphere(bsphere);
    }

    public void createEffect() {

        subdivide();

        RenderState rs = new RenderState();
        rs.clearBuffers(true);
        rs.setClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        rs.enable(GL.GL_DEPTH_TEST);
        rs.setDepthFunc(GL.GL_LESS);
        rs.disable(GL.GL_BLEND);
        rs.enable(GL.GL_CULL_FACE);
        rs.setCullFaces(GL.GL_BACK);
        //rs.setBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE);

        InputStream vertexStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong2.vert");
        InputStream fragmentStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong.frag");
        Pass pass1 = new Pass(vertexStream, fragmentStream);
        pass1.scene = scene;
        pass1.renderState = rs;

        RenderState rs2 = new RenderState();
        rs2.clearBuffers(true);
        rs2.setClearColor(1.0f, 1.0f, 1.0f, 1.0f);

        RenderState rs3 = new RenderState();
        rs3.setDepthFunc(GL.GL_LEQUAL);
        rs3.clearBuffers(false);

        Pass pass2 = new Pass();
        pass2.scene = edgesScene;
        pass2.renderState = rs3;

        Pass pass3 = new Pass();
        pass3.scene = pointsScene;
        pass3.renderState = rs3;

        bSphereProvider = scene;

        effect = new Effect();
        effect.addGLEventListener(pass1);
        effect.addGLEventListener(pass2);
        effect.addGLEventListener(pass3);
    }

    @Override
    public void createUI(ExamplePanel panel) {
        ChangeListener changeListener = new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                subdivide();
                panel.canvas.repaint();
            }
        };
        subdivisions.addChangeListener(changeListener);
        smoothShading.addChangeListener(changeListener);


        showControlGridParam.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                controlGrid.setVisibility(!controlGrid.getVisibility());
                panel.canvas.repaint();
            }
        });
        showEdgesParam.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                edges.setVisibility(!edges.getVisibility());
                panel.canvas.repaint();
            }
        });
        showVerticesParam.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                points.setVisibility(!points.getVisibility());
                panel.canvas.repaint();
            }
        });

        panel.params.add(new ParameterLabel(subdivisions));
        panel.params.add(new IntSlider(subdivisions, JSlider.HORIZONTAL, 1, 4));
        panel.params.add(new ParameterLabel(smoothShading));
        panel.params.add(new BooleanCheckBox("Smooth shading", smoothShading));
        panel.params.add(new BooleanCheckBox("Show control grid", showControlGridParam));
        panel.params.add(new BooleanCheckBox("Show surface edges", showEdgesParam));
        panel.params.add(new BooleanCheckBox("Show surface vertices", showVerticesParam));

    }

    public String getName() {
        return "Catmull-Clark surface";
    }

    public static void main(String[] args) {
        Example example = new SubdivisionSurfaceExample();

        Example example2 = new C0();

        PatchRenderer renderer = new PatchRenderer();
        renderer.start(example2);

        JButton button = new JButton("Change example");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                renderer.setExample(example);
            }
        });
        renderer.w.params.add(button);
    }
}
