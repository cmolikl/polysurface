/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface.subdiv;

import gleem.linalg.Vec3f;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Point4d;
import scene.surface.Appearance;
import scene.surface.mesh.Edge.HalfEdge;
import scene.surface.mesh.Mesh;
import scene.surface.mesh.MeshRenderer;
import scene.surface.mesh.Vertex;

/**
 *
 * @author cmolikl
 */
public class WingedEdgePointRenderer extends MeshRenderer {
    @Override
    public void render(GLAutoDrawable drawable, Mesh mesh) {
        if(!mesh.getVisibility()) return;
        if(mesh.getGeometry() == null) return;
        GL2 gl = drawable.getGL().getGL2();
        gl.glPointSize(10f);
        gl.glDisable(GL2.GL_LIGHTING);
        gl.glUseProgram(0);
        //super.renderMesh(drawable, mesh);
        initCallList(gl, mesh);
        gl.glEnable(GL2.GL_LIGHTING);
        gl.glPointSize(1f);
    }

    @Override
    protected void initCallList(GL2 gl, Mesh mesh) {
        //System.out.println("Initializing mesh " + name);
        
        CatmullClarkSurface cc = (CatmullClarkSurface) mesh;

        //mesh.callList = gl.glGenLists(1);
        Appearance a = mesh.appearance != null ? mesh.appearance : new Appearance(null);
        Vec3f d = a.getDiffuse();

        //gl.glNewList(mesh.callList, GL.GL_COMPILE);
        int drawtype = GL.GL_POINTS;

        
        gl.glBegin(drawtype);
        gl.glColor4d(0f, 0f, 0f, 1.0);
        //for (Edge e : mesh.getGeometry().getEdgeList()) {
            //if(e == null) continue;
//        Vertex v = mesh.geometry.getVertexList().get(0);
//        Point4d loc = v.getLocation();
//        gl.glVertex3d(loc.getX(), loc.getY(), loc.getZ());
//        gl.glColor4d(0f, 1f, 0f, 1.0);
//        for(HalfEdge he : v) {
//            //HalfEdge he = e.getHalfEdge(Edge.DIR);
//            //Point4d loc = he.v0().getLocation();
//            //gl.glVertex3d(loc.getX(), loc.getY(), loc.getZ());
//            loc = he.v1().getLocation();
//            gl.glVertex3d(loc.getX(), loc.getY(), loc.getZ());
//        }
        Point4d loc;
        gl.glColor4d(1f, 0f, 0f, 1.0);
        for(Vertex fv : cc.facePoints.values()) {
            loc = fv.getLocation();
            gl.glVertex3d(loc.getX(), loc.getY(), loc.getZ());
        }
        gl.glColor4d(0f, 1f, 0f, 1.0);
        for(Vertex ev : cc.edgePoints.values()) {
            loc = ev.getLocation();
            gl.glVertex3d(loc.getX(), loc.getY(), loc.getZ());
        }
        gl.glColor4d(0f, 0f, 1f, 1.0);
        for(Vertex vv : cc.vertexPoints.values()) {
            loc = vv.getLocation();
            gl.glVertex3d(loc.getX(), loc.getY(), loc.getZ());
        }
        gl.glEnd();
        //gl.glEndList();
    }
}

