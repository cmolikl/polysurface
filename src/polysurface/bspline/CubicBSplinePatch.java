/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface.bspline;

import javax.vecmath.Point4d;
import javax.vecmath.Vector3d;
import scene.surface.mesh.Face;
import scene.surface.mesh.Mesh;
import scene.surface.mesh.MeshGeometry;
import scene.surface.mesh.Vertex;
import polysurface.Patch;
import polysurface.PatchRenderer;

/**
 *
 * @author cmolikl
 */
public class CubicBSplinePatch implements Patch {

    public Point4d[][] cp;

    public CubicBSplinePatch() {
        cp = new Point4d[4][4];
        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 4; j++) {
                cp[i][j] = new Point4d(i, j, 0, 1);
            }
        }
    }

    public CubicBSplinePatch(Point4d[][] cp) {
        this.cp = cp;
    }

    private double signedVolumeOfTriangle(Point4d p1, Point4d p2, Point4d p3) {
        double v321 = p3.x * p2.y * p1.z;
        double v231 = p2.x * p3.y * p1.z;
        double v312 = p3.x * p1.y * p2.z;
        double v132 = p1.x * p3.y * p2.z;
        double v213 = p2.x * p1.y * p3.z;
        double v123 = p1.x * p2.y * p3.z;
        return (1.0/6.0)*(-v321 + v231 + v312 - v132 - v213 + v123);
    }


    //     0   1   2   3
    //  0  +---+---+---+
    //     |   |   |   |
    //  1  +---+---+---+
    //     |   |   |   |
    //  2  +---+---+---+
    //     |   |   |   |
    //  3  +---+---+---+

    private double getVolume() {
        double volume = 0;
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                volume += signedVolumeOfTriangle(cp[i][j], cp[i+1][j], cp[i][j+1]);
                volume += signedVolumeOfTriangle(cp[i+1][j], cp[i+1][j+1], cp[i][j+1]);
            }
        }
        volume += signedVolumeOfTriangle(cp[0][0], cp[0][3], cp[3][0]);
        volume += signedVolumeOfTriangle(cp[3][0], cp[0][3], cp[3][3]);
        return Math.abs(volume);
    }

    private Point4d li(Point4d p1, Point4d p2, double t) {
        Point4d result = new Point4d();
        result.interpolate(p1, p2, t);
        return result;
    }

    public Point4d[][] deBoor2(Point4d[] p, double t) {
        Point4d[][] result = new Point4d[p.length][];
        result[0] = p;
        for(int k = 1; k < p.length; k++) {
            result[k] = new Point4d[p.length - k];
            int div = p.length-k;
            for(int i = 0; i < p.length-k; i++) {
                double nt = (div-i-1+t)/div;
                result[k][i] = li(result[k-1][i], result[k-1][i+1], nt);
            }
        }
        return result;
    }

    public Point4d[][][] evaluate2(double u, double v) {
        Point4d[][][] result = new Point4d[4][][];
        result[0] = cp;

        Point4d ip1 = new Point4d();
        Point4d ip2 = new Point4d();

        for(int level = 1; level < 4; level++) {
            result[level] = new Point4d[4-level][4-level];
            int div = 4-level;
            for(int i = 0; i < 4-level; i++) {
                double nu = (div-i-1+u)/div;
                for(int j = 0; j < 4-level; j++) {
                    double nv = (div-j-1+v)/div;
                    ip1.interpolate(result[level-1][i][j], result[level-1][i+1][j], nu);
                    ip2.interpolate(result[level-1][i][j+1], result[level-1][i+1][j+1], nu);
                    result[level][i][j] = li(ip1, ip2, nv);
                }
            }
        }
        return result;
    }

    @Override
    public String toString() {
        String s = "";
        for(int i = 0; i < cp.length; i++) {
            for(int j = 0; j < cp[i].length; j++) {
                s += cp[j][i] + " ";
            }
            s+="\n";
        }
        return s;
    }

    private Vector3d getNormal(Point4d p1, Point4d p2, Point4d p3) {
        Vector3d n01 = new Vector3d(
            p2.x - p1.x,
            p2.y - p1.y,
            p2.z - p1.z
        );
        Vector3d n10 = new Vector3d(
            p3.x - p1.x,
            p3.y - p1.y,
            p3.z - p1.z
        );
        n01.normalize();
        n10.normalize();
        n01.cross(n01, n10);
        n01.normalize();
        return n01;
    }


    public void teselate(Mesh mesh, int res, boolean smoothShading) {
        if(mesh.geometry == null) {
            mesh.geometry = new MeshGeometry();
        }
        Vertex[][] result = new Vertex[res+1][res+1];
        Point4d[][][] dc;
        //Point4d[] dc2 = new Point4d[4];
        double delta = 1.0/res;
        for(int i = 0; i <= res; i++) {
            double u = delta*i;
            //find control points of moving curve
            //dc2[0] = deBoor2(cp[0], u)[3][0];
            //dc2[1] = deBoor2(cp[1], u)[3][0];
            //dc2[2] = deBoor2(cp[2], u)[3][0];
            //dc2[3] = deBoor2(cp[3], u)[3][0];
            for(int j = 0; j <= res; j++) {
                double v = delta*j;
                //find point on surface
                //result[i][j] = new Vertex(deBoor2(dc2, v)[3][0]);
                dc = evaluate2(u, v);
                dc[3][0][0].scale(1.0/dc[3][0][0].w);
                result[i][j] = new Vertex(dc[3][0][0]);
                if(smoothShading) {
                    dc[2][0][0].scale(1.0 / dc[2][0][0].w);
                    dc[2][1][0].scale(1.0 / dc[2][1][0].w);
                    dc[2][0][1].scale(1.0 / dc[2][0][1].w);
                    Vector3d n = getNormal(dc[2][0][0], dc[2][1][0], dc[2][0][1]);
                    result[i][j].setNormal(n.x, n.y, n.z);
                }
                mesh.geometry.add(result[i][j]);
            }
        }

        // v0 +------+ v1
        //    |     /|
        //    |    / |
        //    |   /  |
        //    |  /   |
        //    | /    |
        //    |/     |
        // v3 +------+ v2

        for(int i = 0; i < res; i++) {
            for(int j = 0; j < res; j++) {
                Vertex v0 = result[i][j];
                Vertex v1 = result[i+1][j];
                Vertex v2 = result[i+1][j+1];
                Vertex v3 = result[i][j+1];
                if(!smoothShading) {
                    if(i > 0 || j > 0) {
                        Point4d l = result[i][j].getLocation();
                        v0 = new Vertex(l.x, l.y, l.z, l.w);
                    }
                    if(j > 0) {
                        Point4d l = result[i+1][j].getLocation();
                        v1 = new Vertex(l.x, l.y, l.z, l.w);
                    }
                    if(i > 0) {
                        Point4d l = result[i][j+1].getLocation();
                        v3 = new Vertex(l.x, l.y, l.z, l.w);
                    }
                    Vector3d n = getNormal(v0.getLocation(), v1.getLocation(), v3.getLocation());
                    n.normalize();
                    v0.setNormal(n.x, n.y, n.z);
                    v1.setNormal(n.x, n.y, n.z);
                    v2.setNormal(n.x, n.y, n.z);
                    v3.setNormal(n.x, n.y, n.z);
                }
                Face f0 = new Face(v1, v3, v0);
                Face f1 = new Face(v1, v2, v3);
                mesh.geometry.add(f0);
                mesh.geometry.add(f1);
            }
        }
    }

    public void getPoints(Mesh mesh) {
        for(int i = 0; i < 4; i ++) {
            for(int j = 0; j < 4; j++) {
               cp[i][j].scale(1.0/cp[i][j].w);
               mesh.geometry.add(new Vertex(cp[i][j]));
            }
        }
    }

    public void getFaces(Mesh mesh) {
        for(int i = 0; i < 3; i ++) {
            for(int j = 0; j < 3; j++) {
                Vertex v0 = new Vertex(cp[i][j]);
                Vertex v1 = new Vertex(cp[i][j+1]);
                Vertex v2 = new Vertex(cp[i+1][j+1]);
                Vertex v3 = new Vertex(cp[i+1][j]);
                Face f = new Face(v0, v1, v2, v3);
                mesh.geometry.add(f);
                mesh.geometry.add(v0);
                mesh.geometry.add(v1);
                mesh.geometry.add(v2);
                mesh.geometry.add(v3);
            }
        }
    }

//    public static void main(String[] args) {
//
//        CubicBSplinePatch patch0 = new CubicBSplinePatch(new Point4d[][] {
//            {new Point4d(0,0,0,1), new Point4d(1,0,1,1), new Point4d(2,0,1,1), new Point4d(3,0,0,1)},
//            {new Point4d(0,1,1,1), new Point4d(1,1,2,1), new Point4d(2,1,2,1), new Point4d(3,1,1,1)},
//            {new Point4d(0,2,1,1), new Point4d(1,2,2,1), new Point4d(2,2,2,1), new Point4d(3,2,1,1)},
//            {new Point4d(0,3,0,1), new Point4d(1,3,1,1), new Point4d(2,3,1,1), new Point4d(3,3,0,1)}
//        });
//
//        CubicBSplinePatch patch1 = new CubicBSplinePatch(new Point4d[][] {
//            {new Point4d(0,1,1,1), new Point4d(1,1,2,1), new Point4d(2,1,2,1), new Point4d(3,1,1,1)},
//            {new Point4d(0,2,1,1), new Point4d(1,2,2,1), new Point4d(2,2,2,1), new Point4d(3,2,1,1)},
//            {new Point4d(0,3,0,1), new Point4d(1,3,1,1), new Point4d(2,3,1,1), new Point4d(3,3,0,1)},
//            {new Point4d(0,4,1,1), new Point4d(1,4,2,1), new Point4d(2,4,2,1), new Point4d(3,4,1,1)}
//        });
//
//        PatchRenderer renderer = new PatchRenderer();
//        renderer.renderPatches(patch0, patch1);
//    }

}

