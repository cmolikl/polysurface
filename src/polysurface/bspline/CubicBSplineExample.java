package polysurface.bspline;

import gleem.linalg.Vec3f;
import polysurface.Example;
import polysurface.ExamplePanel;
import polysurface.Patch;
import polysurface.PatchRenderer;

import javax.vecmath.Point4d;

public class CubicBSplineExample extends Example {
    public Patch[] getPatches() {
        CubicBSplinePatch patch0 = new CubicBSplinePatch(new Point4d[][] {
                {new Point4d(0,0,0,1), new Point4d(1,0,1,1), new Point4d(2,0,1,1), new Point4d(3,0,0,1)},
                {new Point4d(0,1,1,1), new Point4d(1,1,2,1), new Point4d(2,1,2,1), new Point4d(3,1,1,1)},
                {new Point4d(0,2,1,1), new Point4d(1,2,2,1), new Point4d(2,2,2,1), new Point4d(3,2,1,1)},
                {new Point4d(0,3,0,1), new Point4d(1,3,1,1), new Point4d(2,3,1,1), new Point4d(3,3,0,1)}
        });

        CubicBSplinePatch patch1 = new CubicBSplinePatch(new Point4d[][] {
                {new Point4d(0,1,1,1), new Point4d(1,1,2,1), new Point4d(2,1,2,1), new Point4d(3,1,1,1)},
                {new Point4d(0,2,1,1), new Point4d(1,2,2,1), new Point4d(2,2,2,1), new Point4d(3,2,1,1)},
                {new Point4d(0,3,0,1), new Point4d(1,3,1,1), new Point4d(2,3,1,1), new Point4d(3,3,0,1)},
                {new Point4d(0,4,1,1), new Point4d(1,4,2,1), new Point4d(2,4,2,1), new Point4d(3,4,1,1)}
        });

        return new Patch[] {patch0, patch1};
    }

    public String getName() {
        return "BSpline patches";
    }

    @Override
    public void createUI(ExamplePanel panel) {
        super.createUI(panel);
        panel.camera.center = new Vec3f(1.5f, 2.0f, 1.0f);
        panel.camera.position = new Vec3f(3.5054705f, 1.4144659f, 2.6985984f);
        panel.camera.upvector = new Vec3f(-0.62865055f, 0.08829257f, 0.77265966f);
    }

    public static void main(String[] args) {
        Example example = new CubicBSplineExample();
        PatchRenderer renderer = new PatchRenderer();
        renderer.start(example);
    }
}