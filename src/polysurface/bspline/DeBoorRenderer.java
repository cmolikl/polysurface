/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface.bspline;

import gleem.BSphere;
import gleem.linalg.Vec3f;
import polysurface.Example;
import polysurface.ExamplePanel;
import polysurface.Patch;
import polysurface.PatchRenderer;
import scene.Scene;
import scene.surface.Appearance;
import scene.surface.mesh.*;
import tiger.core.Effect;
import tiger.core.Pass;
import tiger.core.RenderState;
import tiger.core.Window;
import ui.control.FloatSlider;
import ui.control.ParameterLabel;
import ui.parameter.FloatParameter;

import javax.media.opengl.GL;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Point4d;
import java.io.InputStream;

/**
 *
 * @author cmolikl
 */
public class DeBoorRenderer extends Example {

    private class UChangeListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            //JSlider s = (JSlider)e.getSource();
            //u = s.getValue() / 100.0;
            Point4d[][][] p = patch.evaluate2(u.getValue(), v.getValue());
            for(int k = 0; k < p.length; k++) {
                for(int i = 0; i < p[k].length; i++) {
                    for(int j = 0; j < p[k][i].length; j++) {
                        dc[k][i][j].set(p[k][i][j]);
                    }
                }
            }
            canvas.display();
        }
    }

    private class VChangeListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            //JSlider s = (JSlider)e.getSource();
            //v = s.getValue() / 100.0;
            Point4d[][][] p = patch.evaluate2(u.getValue(), v.getValue());
            for(int k = 0; k < p.length; k++) {
                for(int i = 0; i < p[k].length; i++) {
                    for(int j = 0; j < p[k][i].length; j++) {
                        dc[k][i][j].set(p[k][i][j]);
                    }
                }
            }
            canvas.display();
        }
    }

    GLCanvas canvas;
    
    FloatParameter u = new FloatParameter("u", 0.5f);
    FloatParameter v = new FloatParameter("v", 0.5f);
    CubicBSplinePatch patch;
    Point4d[][][] dc;

    //EdgeMeshRenderer edgeRenderer = new EdgeMeshRenderer();
    //PointMeshRenderer pointRenderer = new PointMeshRenderer();

    public Patch[] getPatches() {
        CubicBSplinePatch patch1 = new CubicBSplinePatch(new Point4d[][] {
                {new Point4d(0,-3,0,1), new Point4d(1,-3,1,1), new Point4d(2,-3,1,1), new Point4d(3,-3,0,1)},
                {new Point4d(0,-2,1,1), new Point4d(1,-2,2,1), new Point4d(2,-2,2,1), new Point4d(3,-2,1,1)},
                {new Point4d(0,-1,1,1), new Point4d(1,-1,2,1), new Point4d(2,-1,2,1), new Point4d(3,-1,1,1)},
                {new Point4d(0,0,0,1), new Point4d(1,0,1,1), new Point4d(2,0,1,1), new Point4d(3,0,0,1)}
        });

        return new Patch[] {patch1};
    }

    protected void createEffect() {

        patches = getPatches();
        patch = (CubicBSplinePatch) patches[0];

        Mesh points = new Mesh();
        points.geometry = new MeshGeometry();
        Appearance pa = new Appearance("points");
        pa.setDiffuse(1, 0, 1);
        points.appearance = pa;
        points.setRenderer(pointRenderer);
        Scene pointsScene = new Scene();
        pointsScene.addMesh(points);

        //Mesh points1 = new Mesh();
        /*points1.geometry = new MeshGeometry();
        Appearance pa1 = new Appearance("points1");
        pa1.setDiffuse(0, 0, 1);
        points1.appearance = pa1;
        Scene pointsScene1 = new Scene();
        pointsScene1.addMesh(points1);*/


        Scene facesScene = new Scene();

        Mesh mesh = new Mesh();
        Scene scene = new Scene();
        scene.addMesh(mesh);

        //patch.getPoints(points1);
        BSphere bsphere = null;
        Appearance[] fa = new Appearance[4];
        fa[0] = new Appearance("faces0");
        fa[0].setDiffuse(0, 0, 1);
        fa[1] = new Appearance("faces1");
        fa[1].setDiffuse(0, 1, 0);
        fa[2] = new Appearance("faces2");
        fa[2].setDiffuse(1, 0, 0);
        fa[3] = new Appearance("faces3");
        fa[3].setDiffuse(1, 0, 1);
        dc = patch.evaluate2(u.getValue(), v.getValue());
        for (int k = 0; k < dc.length - 1; k++) {
            Mesh faces = new Mesh();
            faces.name = "faces" + k;
            faces.geometry = new MeshGeometry();
            faces.appearance = fa[k];
            for (int i = 0; i < dc[k].length - 1; i++) {
                for (int j = 0; j < dc[k][i].length - 1; j++) {
                    Vertex v0 = new Vertex(dc[k][i][j]);
                    Vertex v1 = new Vertex(dc[k][i][j + 1]);
                    Vertex v2 = new Vertex(dc[k][i + 1][j + 1]);
                    Vertex v3 = new Vertex(dc[k][i + 1][j]);
                    Face f = new Face(v0, v1, v2, v3);
                    faces.geometry.add(f);
                    faces.geometry.add(v0);
                    faces.geometry.add(v1);
                    faces.geometry.add(v2);
                    faces.geometry.add(v3);
                }
            }
            if (k == 0) {
                bsphere = MeshUtils.getBoundingSphere(faces);
                facesScene.setBoundingSphere(bsphere);
            }
            faces.setRenderer(edgeRenderer);
            facesScene.addMesh(faces);
        }
        points.getGeometry().add(new Vertex(dc[3][0][0]));
        patch.teselate(mesh, 10, true);

        RenderState rs = new RenderState();
        rs.clearBuffers(true);
        rs.setClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        rs.enable(GL.GL_DEPTH_TEST);
        rs.disable(GL.GL_BLEND);

        InputStream vertexStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong2.vert");
        InputStream fragmentStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong.frag");
        Pass pass1 = new Pass(vertexStream, fragmentStream);
        pass1.scene = scene;
        pass1.renderState = rs;

        rs = new RenderState();
        rs.clearBuffers(false);

        Pass pass2 = new Pass();
        pass2.scene = facesScene;
        pass2.renderState = rs;

        Pass pass3 = new Pass();
        pass3.scene = pointsScene;
        pass3.renderState = rs;

        /*Pass pass4 = new PointRenderPass();
        pass4.scene = pointsScene1;
        pass4.renderState = rs;*/

        effect = new Effect();
        effect.addGLEventListener(pass1);
        effect.addGLEventListener(pass3);
        //effect.addGLEventListener(pass4);
        effect.addGLEventListener(pass2);

        bSphereProvider = facesScene;

//        Window w = new Window(facesScene, 512, 512);
//        w.setEffect(effect);
//        w.build();
    }

    @Override
    public void createUI(ExamplePanel w) {
        //super.createUI(w);
        canvas = w.canvas;

        ParameterLabel uLabel = new ParameterLabel(u);
        w.params.add(uLabel);

        FloatSlider uParam = new FloatSlider(u, true, JSlider.HORIZONTAL, 0f, 1f);
        u.addChangeListener(new UChangeListener());
        w.params.add(uParam);

        ParameterLabel vLabel = new ParameterLabel(v);
        w.params.add(vLabel);

        FloatSlider vParam = new FloatSlider(v, true, JSlider.HORIZONTAL, 0f, 1f);
        v.addChangeListener(new VChangeListener());
        w.params.add(vParam);

        w.camera.center = new Vec3f(1.5f, -1.5f, 1.0f);
        w.camera.position = new Vec3f(2.3839397f, -3.3273306f, 2.1745229f);
        w.camera.upvector = new Vec3f(-0.14188471f, 0.48574552f, 0.8625079f);
    }

    public String getName() {
        return "DeBoor algorithm";
    }

    public static void main(String[] args) {

        Example example = new DeBoorRenderer();

        PatchRenderer renderer = new PatchRenderer();
        renderer.start(example);
    }

}
