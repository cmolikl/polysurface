/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface.bezier_triangle;

import java.util.Arrays;
import javax.vecmath.Point4d;
import javax.vecmath.Vector3d;

import polysurface.Patch;
import scene.surface.mesh.Face;
import scene.surface.mesh.Mesh;
import scene.surface.mesh.MeshGeometry;
import scene.surface.mesh.Vertex;

/**
 *
 * @author cmolikl
 */
public class CubicBezierTriangle implements Patch {
    public Point4d[] cp;

    double u = 0.5;
    double v = 0.5;
    double w = 0;

    public CubicBezierTriangle(Point4d[] cp) {
        this.cp = cp;
    }

    private double signedVolumeOfTriangle(Point4d p1, Point4d p2, Point4d p3) {
        double v321 = p3.x * p2.y * p1.z;
        double v231 = p2.x * p3.y * p1.z;
        double v312 = p3.x * p1.y * p2.z;
        double v132 = p1.x * p3.y * p2.z;
        double v213 = p2.x * p1.y * p3.z;
        double v123 = p1.x * p2.y * p3.z;
        return (1.0/6.0)*(-v321 + v231 + v312 - v132 - v213 + v123);
    }


    //     0   1   2   3
    //  0  +---+---+---+
    //     |   |   |   |
    //  1  +---+---+---+
    //     |   |   |   |
    //  2  +---+---+---+
    //     |   |   |   |
    //  3  +---+---+---+

    private Point4d evaluate(double u, double v, double w) {
        Point4d[][] result = deCasteljau(u, v, w);
        return result[3][0];
    }

    private Point4d li(Point4d p1, Point4d p2, double t) {
        Point4d result = new Point4d();
        result.interpolate(p1, p2, t);
        return result;
    }

    public Point4d[][] deCasteljau(double u, double v, double w) {
        Point4d[][] result = new Point4d[4][];
        result[0] = cp;
        result[1] = new Point4d[6];
        result[2] = new Point4d[3];
        result[3] = new Point4d[1];

        for(int level = 1; level < 4; level++) {
            int i = 0;
            for(int j = 0; j < 4 - level; j++) {
                for(int k = 0; k < 4 - level - j; k++) {
                    result[level][i] = new Point4d();
                    Point4d p = (Point4d) result[level-1][k + j*(5-level) - j/2].clone();
                    p.scale(u);
                    result[level][i].add(p);
                    p = (Point4d) result[level-1][k + j*(5-level) - j/2 + 1].clone();
                    p.scale(v);
                    result[level][i].add(p);
                    p = (Point4d) result[level-1][k + j*(5-level) - j/2 + 4-j - level+1].clone();
                    p.scale(w);
                    result[level][i].add(p);
                    i++;
                }
            }
        }

        return result;
    }

    public CubicBezierTriangle[] subdivide(double u, double v, double w) {
        Point4d[][] dc = deCasteljau(u, v, w);

        CubicBezierTriangle bt0 = new CubicBezierTriangle(new Point4d[] {
            dc[0][0], dc[0][1], dc[0][2], dc[0][3],
            dc[1][0], dc[1][1], dc[1][2],
            dc[2][0], dc[2][1],
            dc[3][0]
        });

        CubicBezierTriangle bt1 = new CubicBezierTriangle(new Point4d[] {
            dc[0][9], dc[0][7], dc[0][4], dc[0][0],
            dc[1][5], dc[1][3], dc[1][0],
            dc[2][2], dc[2][0],
            dc[3][0]
        });

        CubicBezierTriangle bt2 = new CubicBezierTriangle(new Point4d[] {
            dc[0][3], dc[0][6], dc[0][8], dc[0][9],
            dc[1][2], dc[1][4], dc[1][5],
            dc[2][1], dc[2][2],
            dc[3][0]
        });

        CubicBezierTriangle[] result = new CubicBezierTriangle[] {
            bt0, bt1, bt2
        };
        return result;
    }

    public String toString() {
        String s = "";
        for(int i = 0; i < cp.length; i++) {
            s += cp[i] + " ";
        }
        s +="\n";
        return s;
    }

    private Vector3d getNormal(Point4d p1, Point4d p2, Point4d p3) {
        Vector3d n01 = new Vector3d(
            p2.x - p1.x,
            p2.y - p1.y,
            p2.z - p1.z
        );
        Vector3d n10 = new Vector3d(
            p3.x - p1.x,
            p3.y - p1.y,
            p3.z - p1.z
        );
        n01.normalize();
        n10.normalize();
        n01.cross(n01, n10);
        n01.normalize();
        return n01;
    }


    public void teselate(Mesh mesh, int res, boolean smoothShading) {
        if(mesh.geometry == null) {
            mesh.geometry = new MeshGeometry();
        }
        if(res == 0) {
            Vertex v0 = new Vertex(cp[0]);
            Vector3d n = getNormal(cp[0], cp[1], cp[4]);
            v0.setNormal(n.x, n.y, n.z);

            Vertex v1 = new Vertex(cp[3]);
            n = getNormal(cp[3], cp[6], cp[2]);
            v1.setNormal(n.x, n.y, n.z);

            Vertex v2 = new Vertex(cp[9]);
            n = getNormal(cp[9], cp[7], cp[8]);
            v2.setNormal(n.x, n.y, n.z);

            Face f0 = new Face(v0, v1, v2);
            mesh.geometry.add(v0);
            mesh.geometry.add(v1);
            mesh.geometry.add(v2);
            mesh.geometry.add(f0);
        }
        else {
            CubicBezierTriangle[] subpatch = subdivide(u, v, w);
            for(int i = 1; i < subpatch.length; i++)
                subpatch[i].teselate(mesh, res-1, smoothShading);
        }
    }

     public void getPoints(Mesh mesh) {
        for(int i = 0; i < 10; i ++) {
               mesh.geometry.add(new Vertex(cp[i]));
        }
    }

    public void getFaces(Mesh mesh) {
        int level = 1;
        int i = 0;
        for(int j = 0; j < 4 - level; j++) {
            for(int k = 0; k < 4 - level - j; k++) {
                Vertex v0 = new Vertex(cp[k + j*(5-level) - j/2]);
                Vertex v1 = new Vertex(cp[k + j*(5-level) - j/2 + 1]);
                Vertex v2 = new Vertex(cp[k + j*(5-level) - j/2 + 4-j - level+1]);
                Face f = new Face(v0, v1, v2);
                mesh.geometry.add(f);
                mesh.geometry.add(v0);
                mesh.geometry.add(v1);
                mesh.geometry.add(v2);
                i++;
            }
        }
    }

    public static void main(String[] args) {
        CubicBezierTriangle bt = new CubicBezierTriangle(new Point4d[] {
            new Point4d(0,0,0,1), new Point4d(1,0,1,1), new Point4d(2,0,1,1), new Point4d(3,0,0,1),
            new Point4d(0.5,1,1,1), new Point4d(1.5,1,2,1), new Point4d(2.5,1,1,1),
            new Point4d(1,2,1,1), new Point4d(2,2,1,1),
            new Point4d(1.5,3,0,1)
        });

        Point4d[][] dc = bt.deCasteljau(1.0/3.0, 1.0/3.0, 1.0/3.0);
        for(int i = 0; i < dc.length; i++) {
            System.out.println(Arrays.toString(dc[i]));
        }
        
    }
}
