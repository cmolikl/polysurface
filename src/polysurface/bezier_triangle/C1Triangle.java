/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface.bezier_triangle;

import polysurface.Patch;
import tiger.example.*;
import gleem.BSphere;
import java.io.InputStream;
import javax.media.opengl.GL;
import javax.vecmath.Point4d;
import scene.surface.Appearance;
import scene.surface.mesh.Mesh;
import scene.surface.mesh.MeshGeometry;
import scene.surface.mesh.MeshUtils;
import scene.Scene;
import tiger.core.Effect;
import tiger.core.Pass;
import tiger.core.RenderState;
import tiger.core.Window;

/**
 *
 * @author cmolikl
 */
public class C1Triangle {

    EdgeMeshRenderer edgeRenderer = new EdgeMeshRenderer();
    PointMeshRenderer pointRenderer = new PointMeshRenderer();

    public void renderPatches(Patch[] patches) {

        Mesh points = new Mesh();
        points.geometry = new MeshGeometry();
        Appearance pa1 = new Appearance("points");
        pa1.setDiffuse(0, 0, 0);
        points.appearance = pa1;
        Scene pointsScene1 = new Scene();
        points.setRenderer(pointRenderer);
        pointsScene1.addMesh(points);

        Appearance[] fa = new Appearance[4];
        fa[0] = new Appearance("faces0");
        fa[0].setDiffuse(0, 0, 1);
        fa[1] = new Appearance("faces1");
        fa[1].setDiffuse(0, 1, 0);
        fa[2] = new Appearance("faces2");
        fa[2].setDiffuse(1, 0, 0);
        fa[3] = new Appearance("faces3");
        fa[3].setDiffuse(1, 0, 1);
        Scene facesScene = new Scene();
        Mesh[] faces = new Mesh[4];
        for(int i = 0; i < patches.length; i++) {
            faces[i] = new Mesh();
            faces[i].name = "faces" + i;
            faces[i].geometry = new MeshGeometry();
            faces[i].appearance = fa[i];
            faces[i].setRenderer(edgeRenderer);
            facesScene.addMesh(faces[i]);
        }

        Mesh mesh = new Mesh();
        Scene scene = new Scene();
        scene.addMesh(mesh);

        int i = 0;
        for(Patch patch : patches) {
            patch.getPoints(points);
            patch.getFaces(faces[i]);
            patch.teselate(mesh, 10, true);
            i++;
        }

        BSphere bsphere = MeshUtils.getBoundingSphere(mesh);
        scene.setBoundingSphere(bsphere);

        RenderState rs = new RenderState();
        rs.clearBuffers(true);
        rs.setClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        rs.enable(GL.GL_DEPTH_TEST);
        rs.disable(GL.GL_BLEND);
        //rs.setBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE);

        InputStream vertexStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong2.vert");
        InputStream fragmentStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong.frag");
        Pass pass1 = new Pass(vertexStream, fragmentStream);
        pass1.scene = scene;
        pass1.renderState = rs;

        rs = new RenderState();
        rs.clearBuffers(false);
        //rs.disable(GL.GL_DEPTH_TEST);
        //rs.enable(GL.GL_BLEND);
        //rs.setBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE);

        Pass pass2 = new Pass();
        pass2.scene = facesScene;
        pass2.renderState = rs;

        Pass pass3 = new Pass();
        pass3.scene = pointsScene1;
        pass3.renderState = rs;

        Effect effect = new Effect();
        effect.addGLEventListener(pass1);
        effect.addGLEventListener(pass2);
        effect.addGLEventListener(pass3);

        Window w = new Window(scene, 512, 512);
        w.setEffect(effect);
        //w.runFastAsPosible = true;
        w.start();

    }

    public static void main(String[] args) {

        CubicBezierTriangle bt1 = new CubicBezierTriangle(new Point4d[] {
            new Point4d(0,0,0,1), new Point4d(1,0,1,1), new Point4d(2,0,1,1), new Point4d(3,0,0,1),
            new Point4d(0.5,1,1,1), new Point4d(1.5,1,2,1), new Point4d(2.5,1,1,1),
            new Point4d(1,2,1,1), new Point4d(2,2,1,1),
            new Point4d(1.5,3,0,1)
        });

        CubicBezierTriangle bt2 = new CubicBezierTriangle(new Point4d[] {
            new Point4d(0,0,0,1), new Point4d(1,0,1,1), new Point4d(2,0,1,1), new Point4d(3,0,0,1),
            new Point4d(0.5,-1,0,1), new Point4d(1.5,-1,0,1), new Point4d(2.5,-1,0,1),
            new Point4d(1,-2,-1,1), new Point4d(2,-2,-1,1),
            new Point4d(1.5,-3,0,1)
        });

        C1Triangle renderer = new C1Triangle();
        renderer.renderPatches(new Patch[] {bt1, bt2});
    }
}
