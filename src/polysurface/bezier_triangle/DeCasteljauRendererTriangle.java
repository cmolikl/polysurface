/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface.bezier_triangle;

import gleem.BSphere;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.io.InputStream;
import javax.media.opengl.GL;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Point4d;
import scene.surface.Appearance;
import scene.surface.mesh.Face;
import scene.surface.mesh.Mesh;
import scene.surface.mesh.MeshGeometry;
import scene.surface.mesh.MeshUtils;
import scene.Scene;
import scene.surface.mesh.Vertex;
import tiger.core.Effect;
import tiger.core.Pass;
import tiger.core.RenderState;
import tiger.core.Window;
import tiger.example.EdgeMeshRenderer;
import tiger.example.PointMeshRenderer;

/**
 *
 * @author cmolikl
 */
public class DeCasteljauRendererTriangle {

    EdgeMeshRenderer edgeRenderer = new EdgeMeshRenderer();
    PointMeshRenderer pointRenderer = new PointMeshRenderer();

    private class UChangeListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            JSlider s = (JSlider)e.getSource();
            if(s.hasFocus()) {
                double us = s.getValue() / 100.0;
                double diff = (u - us)* 0.5;
                u = us;
                v += diff;
                w += diff;
                vParam.setValue((int) (v*100));
                wParam.setValue((int) (w*100));
                Point4d[][] p = patch.deCasteljau(u, v, w);
                for(int k = 0; k < p.length; k++) {
                    for(int i = 0; i < p[k].length; i++) {
                        dc[k][i].set(p[k][i]);
                    }
                }
                canvas.display();
            }
        }
    }

    private class VChangeListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            JSlider s = (JSlider)e.getSource();
            if(s.hasFocus()) {
                double vs = s.getValue() / 100.0;
                double diff = (v - vs)* 0.5;
                v = vs;
                u += diff;
                w += diff;
                uParam.setValue((int) (u*100));
                wParam.setValue((int) (w*100));
                Point4d[][] p = patch.deCasteljau(u, v, w);
                for(int k = 0; k < p.length; k++) {
                    for(int i = 0; i < p[k].length; i++) {
                        dc[k][i].set(p[k][i]);
                    }
                }
                canvas.display();
            }
        }
    }

    private class WChangeListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            JSlider s = (JSlider)e.getSource();
            if(s.hasFocus()) {
                double ws = s.getValue() / 100.0;
                double diff = (w - ws)* 0.5;
                w = ws;
                u += diff;
                v += diff;
                uParam.setValue((int) (u*100));
                vParam.setValue((int) (v*100));
                Point4d[][] p = patch.deCasteljau(u, v, w);
                for(int k = 0; k < p.length; k++) {
                    for(int i = 0; i < p[k].length; i++) {
                        dc[k][i].set(p[k][i]);
                    }
                }
                canvas.display();
            }
        }
    }

    GLCanvas canvas;

    double u = 1.0/3.0;
    double v = 1.0/3.0;
    double w = 1.0/3.0;
    CubicBezierTriangle patch;
    Point4d[][] dc;
    JSlider uParam, vParam, wParam;

    public void start(CubicBezierTriangle patch) {

        this.patch = patch;

        Mesh points = new Mesh();
        points.geometry = new MeshGeometry();
        Appearance pa = new Appearance("points");
        pa.setDiffuse(1, 0, 1);
        points.appearance = pa;
        Scene pointsScene = new Scene();
        points.setRenderer(pointRenderer);
        pointsScene.addMesh(points);

        //Mesh points1 = new Mesh();
        /*points1.geometry = new MeshGeometry();
        Appearance pa1 = new Appearance("points1");
        pa1.setDiffuse(0, 0, 1);
        points1.appearance = pa1;
        Scene pointsScene1 = new Scene();
        pointsScene1.addMesh(points1);*/



        Scene facesScene = new Scene();

        Mesh mesh = new Mesh();
        Scene scene = new Scene();
        scene.addMesh(mesh);

        //patch.getPoints(points1);

        Appearance[] fa = new Appearance[4];
        fa[0] = new Appearance("faces0");
        fa[0].setDiffuse(0, 0, 1);
        fa[1] = new Appearance("faces1");
        fa[1].setDiffuse(0, 1, 0);
        fa[2] = new Appearance("faces2");
        fa[2].setDiffuse(1, 0, 0);
        fa[3] = new Appearance("faces3");
        fa[3].setDiffuse(1, 0, 1);
        dc = patch.deCasteljau(u, v, w);
        for(int level = 1; level < dc.length; level++) {
            Mesh faces = new Mesh();
            faces.name = "faces" + level;
            faces.geometry = new MeshGeometry();
            faces.appearance = fa[level-1];
            for(int j = 0; j < 4 - level; j++) {
                for(int k = 0; k < 4 - level - j; k++) {
                    Vertex v0 = new Vertex(dc[level-1][k + j*(5-level) - j/2]);
                    Vertex v1 = new Vertex(dc[level-1][k + j*(5-level) - j/2 + 1]);
                    Vertex v2 = new Vertex(dc[level-1][k + j*(5-level) - j/2 + 4-j - level+1]);
                    Face f = new Face(v0, v1, v2);
                    faces.geometry.add(f);
                    faces.geometry.add(v0);
                    faces.geometry.add(v1);
                    faces.geometry.add(v2);
                }
            }
            faces.setRenderer(edgeRenderer);
            facesScene.addMesh(faces);
        }
        points.getGeometry().add(new Vertex(dc[3][0]));
        patch.teselate(mesh, 10, true);

        BSphere bsphere = MeshUtils.getBoundingSphere(mesh);
        scene.setBoundingSphere(bsphere);

        RenderState rs = new RenderState();
        rs.clearBuffers(true);
        rs.setClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        rs.enable(GL.GL_DEPTH_TEST);
        rs.disable(GL.GL_BLEND);

        InputStream vertexStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong2.vert");
        InputStream fragmentStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong.frag");
        Pass pass1 = new Pass(vertexStream, fragmentStream);
        pass1.scene = scene;
        pass1.renderState = rs;

        rs = new RenderState();
        rs.clearBuffers(false);

        Pass pass2 = new Pass();
        pass2.scene = facesScene;
        pass2.renderState = rs;

        Pass pass3 = new Pass();
        pass3.scene = pointsScene;
        pass3.renderState = rs;

        /*Pass pass4 = new PointRenderPass();
        pass4.scene = pointsScene1;
        pass4.renderState = rs;*/

        Effect effect = new Effect();
        effect.addGLEventListener(pass1);
        effect.addGLEventListener(pass3);
        //effect.addGLEventListener(pass4);
        effect.addGLEventListener(pass2);

        Window w = new Window(scene, 512, 512);
        w.setEffect(effect);
        w.build();

        canvas = w.canvas;

        JPanel panel = new JPanel(new GridLayout(3, 1));

        uParam = new JSlider(JSlider.HORIZONTAL, 0, 100, 33);
        uParam.addChangeListener(new UChangeListener());
        panel.add(uParam);

        vParam = new JSlider(JSlider.HORIZONTAL, 0, 100, 33);
        vParam.addChangeListener(new VChangeListener());
        panel.add(vParam);
        //w.frame.getContentPane().add(vParam, BorderLayout.SOUTH);

        wParam = new JSlider(JSlider.HORIZONTAL, 0, 100, 33);
        wParam.addChangeListener(new WChangeListener());
        panel.add(wParam);
        
        w.frame.getContentPane().add(panel, BorderLayout.SOUTH);

        w.run();
    }

    public static void main(String[] args) {

        CubicBezierTriangle bt1 = new CubicBezierTriangle(new Point4d[] {
            new Point4d(0,0,0,1), new Point4d(1,0,1,1), new Point4d(2,0,1,1), new Point4d(3,0,0,1),
            new Point4d(0.5,1,1,1), new Point4d(1.5,1,2,1), new Point4d(2.5,1,1,1),
            new Point4d(1,2,1,1), new Point4d(2,2,1,1),
            new Point4d(1.5,3,0,1)
        });

        DeCasteljauRendererTriangle renderer = new DeCasteljauRendererTriangle();
        renderer.start(bt1);
    }

}
