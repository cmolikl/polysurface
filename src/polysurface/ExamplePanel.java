package polysurface;

import gleem.BSphereProvider;
import polysurface.bezier.C0;
import tiger.core.*;
import tiger.core.Window;

import javax.media.opengl.DebugGL3;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.awt.*;

public class ExamplePanel extends JPanel {
    public GLCanvas canvas;
    public JPanel params;
    public OrthogonalCamera camera;

    public ExamplePanel(Example example) {

        Effect effect = example.getEffect();
        BSphereProvider bSphereProvider = example.getBSphereProvider();

        camera = new OrthogonalCamera(effect, bSphereProvider);
        OrthogonalCamera.CameraZoomListener zoomListener = camera.new CameraZoomListener();
        OrthogonalCamera.CameraOrbitListener orbitListener = camera.new CameraOrbitListener();

        ViewPort viewPort = new ViewPort(0f,0f, 1f,1f, camera);

        GLCapabilities capabilities = new GLCapabilities(GLProfile.getDefault());
        capabilities.setSampleBuffers(true);
        capabilities.setNumSamples(4);
        capabilities.setDoubleBuffered(true);

        canvas = new GLCanvas(capabilities);
        Dimension dimension = new Dimension(800, 800);
        canvas.setPreferredSize(dimension);
        canvas.setMinimumSize(dimension);
        canvas.addGLEventListener(viewPort);
        canvas.addMouseWheelListener(zoomListener);
        canvas.addMouseListener(orbitListener);
        canvas.addMouseMotionListener(orbitListener);

        camera.addChangeListener((ChangeEvent e) -> {canvas.repaint();});

        params = new JPanel();
        Dimension size = new Dimension(200, 800);
        params.setMaximumSize(size);
        params.setMinimumSize(size);
        params.setPreferredSize(size);

        example.createUI(this);

        SpringLayout layout1 = new SpringLayout();
        this.setLayout(layout1);
        this.add(canvas);
        this.add(params);
        layout1.putConstraint(SpringLayout.WEST, canvas, 0, SpringLayout.WEST, this);
        layout1.putConstraint(SpringLayout.NORTH, canvas, 0, SpringLayout.NORTH, this);
        layout1.putConstraint(SpringLayout.SOUTH, canvas, 0, SpringLayout.SOUTH, this);
        layout1.putConstraint(SpringLayout.EAST, params, 0, SpringLayout.EAST, this);
        layout1.putConstraint(SpringLayout.NORTH, params, 0, SpringLayout.NORTH, this);
        layout1.putConstraint(SpringLayout.SOUTH, params, 0, SpringLayout.SOUTH, this);
        layout1.putConstraint(SpringLayout.EAST, canvas, 0, SpringLayout.WEST, params);
    }

    public static void main(String... args) {
        Example example = new C0();
        ExamplePanel examplePanel = new ExamplePanel(example);
        JFrame frame = new JFrame("PolySurface");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(examplePanel);
        frame.pack();
        frame.setVisible(true);
    }
}
