/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface;

import gleem.ExaminerViewer;
import gleem.linalg.Rotf;
import polysurface.bezier.CubicBezierPatch;
import polysurface.bezier_triangle.CubicBezierTriangle;
import polysurface.bezier.QuadraticBezierPatch;
import javax.vecmath.Point4d;
import polysurface.bezier.C0;
import polysurface.bezier.C1;
import scene.Scene;
import tiger.core.Effect;
import tiger.core.GLPanel;
import tiger.core.Window;

/**
 *
 * @author cmolikl
 */
public class PatchRenderer {

    public Window w;
    
    public PatchRenderer() {
        w = new Window(512, 512);
        w.build();
    }
    
    public void start(Example example) {
        w.bSphereProvider = example.getBSphereProvider();
        w.setEffect(example.getEffect());
        //example.createUI(w.params);
        w.run();
    }

    public void setExample(Example example) {
        //TODO Destroy the current effect
        w.bSphereProvider = example.getBSphereProvider();
        Effect effect = example.getEffect();
        w.setEffect(effect);

        ExaminerViewer viewer = w.getControler();
        viewer.detach();
        viewer.attach(w.canvas, (Scene) example.getBSphereProvider());
        viewer.setOrientation(new Rotf());
        viewer.viewAll(w.canvas.getGL());
        w.canvas.repaint();
    }

    public static void main(String[] args) {

        CubicBezierPatch patch1 = new CubicBezierPatch(new Point4d[][] {
            {new Point4d(0,-3,0,1), new Point4d(1,-3,1,1), new Point4d(2,-3,1,1), new Point4d(3,-3,0,1)},
            {new Point4d(0,-2,1,1), new Point4d(1,-2,2,1), new Point4d(2,-2,2,1), new Point4d(3,-2,1,1)},
            {new Point4d(0,-1,1,1), new Point4d(1,-1,2,1), new Point4d(2,-1,2,1), new Point4d(3,-1,1,1)},
            {new Point4d(0,0,0,1), new Point4d(1,0,1,1), new Point4d(2,0,1,1), new Point4d(3,0,0,1)}
        });

        CubicBezierPatch patch2 = new CubicBezierPatch(new Point4d[][] {
            {new Point4d(0,0,0,1), new Point4d(1,0,1,1), new Point4d(2,0,1,1), new Point4d(3,0,0,1)},
            {new Point4d(0,1,-1,1), new Point4d(1,1,0,1), new Point4d(2,1,0,1), new Point4d(3,1,-1,1)},
            {new Point4d(0,2,-1,1), new Point4d(1,2,0,1), new Point4d(2,2,0,1), new Point4d(3,2,-1,1)},
            {new Point4d(0,3,0,1), new Point4d(1,3,1,1), new Point4d(2,3,1,1), new Point4d(3,3,0,1)}
        });

        CubicBezierPatch patch3 = new CubicBezierPatch(new Point4d[][] {
            {new Point4d(3,-3,0,1), new Point4d(4,-3,-1,1), new Point4d(5,-3,-1,1), new Point4d(6,-3,0,1)},
            {new Point4d(3,-2,1,1), new Point4d(4,-2,0,1), new Point4d(5,-2,0,1), new Point4d(6,-2,1,1)},
            {new Point4d(3,-1,1,1), new Point4d(4,-1,0,1), new Point4d(5,-1,0,1), new Point4d(6,-1,1,1)},
            {new Point4d(3,0,0,1), new Point4d(4,0,-1,1), new Point4d(5,0,-1,1), new Point4d(6,0,0,1)}
        });

        CubicBezierPatch patch4 = new CubicBezierPatch(new Point4d[][] {
            {new Point4d(3,0,0,1), new Point4d(4,0,-1,1), new Point4d(5,0,-1,1), new Point4d(6,0,0,1)},
            {new Point4d(3,1,-1,1), new Point4d(4,1,-2,1), new Point4d(5,1,-2,1), new Point4d(6,1,-1,1)},
            {new Point4d(3,2,-1,1), new Point4d(4,2,-2,1), new Point4d(5,2,-2,1), new Point4d(6,2,-1,1)},
            {new Point4d(3,3,0,1), new Point4d(4,3,-1,1), new Point4d(5,3,-1,1), new Point4d(6,3,0,1)}
        });

        QuadraticBezierPatch patch = new QuadraticBezierPatch(new Point4d[][] {
            {new Point4d(0,-2,0,1), new Point4d(1.5,-2,1,1), new Point4d(3,-2,0,1)},
            {new Point4d(0,-1,1,1), new Point4d(1.5,-1,2,1), new Point4d(3,-1,1,1)},
            {new Point4d(0,0,0,1), new Point4d(1.5,0,1,1), new Point4d(3,0,0,1)}
        });


        CubicBezierTriangle bt1 = new CubicBezierTriangle(new Point4d[] {
            new Point4d(0,0,0,1), new Point4d(1,0,0.66666,1), new Point4d(2,0,0.66666,1), new Point4d(3,0,0,1),
            new Point4d(0,1,-1,1), new Point4d(1.5,1,0,1), new Point4d(3,1,-1,1),
            new Point4d(1,2,-1,1), new Point4d(2,2,-1,1),
            new Point4d(1.5,3,0,1)
        });

        PatchRenderer renderer = new PatchRenderer();
        
        Example example = new C0();
        renderer.start(example);

        PatchRenderer renderer1 = new PatchRenderer();
        
        Example example1 = new C1();
        renderer1.start(example1);

        
    }

}
