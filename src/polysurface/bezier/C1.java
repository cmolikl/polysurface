/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface.bezier;

import gleem.linalg.Vec3f;
import polysurface.Example;
import polysurface.ExamplePanel;
import polysurface.Patch;
import javax.vecmath.Point4d;
import tiger.core.Window;

/**
 *
 * @author cmolikl
 */
public class C1 extends Example {

    public Patch[] getPatches() {
        CubicBezierPatch patch1 = new CubicBezierPatch(new Point4d[][] {
                {new Point4d(0,0,0,1), new Point4d(1,0,1,1), new Point4d(2,0,1,1), new Point4d(3,0,0,1)},
                {new Point4d(0,1,1,1), new Point4d(1,1,2,1), new Point4d(2,1,2,1), new Point4d(3,1,1,1)},
                {new Point4d(0,2,1,1), new Point4d(1,2,2,1), new Point4d(2,2,2,1), new Point4d(3,2,1,1)},
                {new Point4d(0,3,0,1), new Point4d(1,3,1,1), new Point4d(2,3,1,1), new Point4d(3,3,0,1)}
        });

        CubicBezierPatch patch2 = new CubicBezierPatch(new Point4d[][] {
                {new Point4d(3,0,0,1), new Point4d(4,0,-1,1), new Point4d(5,0,-1,1), new Point4d(6,0,0,1)},
                {new Point4d(3,1,1,1), new Point4d(4,1,0,1), new Point4d(5,1,0,1), new Point4d(6,1,1,1)},
                {new Point4d(3,2,1,1), new Point4d(4,2,0,1), new Point4d(5,2,0,1), new Point4d(6,2,1,1)},
                {new Point4d(3,3,0,1), new Point4d(4,3,-1,1), new Point4d(5,3,-1,1), new Point4d(6,3,0,1)}
        });

        return new Patch[] {patch1, patch2};
    }

    public String getName() {
        return "C1 Bezier patches";
    }

    @Override
    public void createUI(ExamplePanel panel) {
        super.createUI(panel);
        panel.camera.center = new Vec3f(3.0f, 1.5f, 0.5f);
        panel.camera.position = new Vec3f(5.510308f, -0.4415871f, 1.695236f);
        panel.camera.upvector = new Vec3f(-0.25739685f, 0.24295303f, 0.9352651f);
    }
}
