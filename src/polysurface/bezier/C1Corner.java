/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface.bezier;

import polysurface.Patch;

import javax.vecmath.Point4d;
import polysurface.Example;
import polysurface.PatchRenderer;

/**
 *
 * @author cmolikl
 */
public class C1Corner extends Example {
    
    @Override
    public Patch[] getPatches() {
        if(patches.length == 0) {
            CubicBezierPatch patch1 = new CubicBezierPatch(new Point4d[][] {
                {new Point4d(0,-3,0,1), new Point4d(1,-3,1,1), new Point4d(2,-3,1,1), new Point4d(3,-3,0,1)},
                {new Point4d(0,-2,1,1), new Point4d(1,-2,2,1), new Point4d(2,-2,2,1), new Point4d(3,-2,1,1)},
                {new Point4d(0,-1,1,1), new Point4d(1,-1,2,1), new Point4d(2,-1,2,1), new Point4d(3,-1,1,1)},
                {new Point4d(0,0,0,1), new Point4d(1,0,1,1), new Point4d(2,0,1,1), new Point4d(3,0,0,1)}
            });

            CubicBezierPatch patch2 = new CubicBezierPatch(new Point4d[][] {
                {new Point4d(3,0,0,1), new Point4d(4,0,-1,1), new Point4d(5,0,-1,1), new Point4d(6,0,0,1)},
                {new Point4d(3,1,-1,1), new Point4d(4,1,-2,1), new Point4d(5,1,-2,1), new Point4d(6,1,-1,1)},
                {new Point4d(3,2,-1,1), new Point4d(4,2,-2,1), new Point4d(5,2,-2,1), new Point4d(6,2,-1,1)},
                {new Point4d(3,3,0,1), new Point4d(4,3,-1,1), new Point4d(5,3,-1,1), new Point4d(6,3,0,1)}
            });
            patches = new Patch[] {patch1, patch2};
        }
        return patches;
    }

    public String getName() {
        return "C1 Bezier patches (corner)";
    }

    public static void main(String[] args) {
        Example example = new C1Corner();
        PatchRenderer renderer = new PatchRenderer();
        renderer.start(example);
    }
}
