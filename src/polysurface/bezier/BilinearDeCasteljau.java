/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface.bezier;

import gleem.BSphere;
import java.io.InputStream;
import javax.media.opengl.GL;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Point4d;

import gleem.linalg.Vec3f;
import polysurface.Example;
import polysurface.ExamplePanel;
import polysurface.Patch;
import polysurface.PatchRenderer;
import scene.surface.Appearance;
import scene.surface.mesh.Face;
import scene.surface.mesh.Mesh;
import scene.surface.mesh.MeshGeometry;
import scene.surface.mesh.MeshUtils;
import scene.Scene;
import scene.surface.mesh.Vertex;
import tiger.core.Effect;
import tiger.core.Pass;
import tiger.core.RenderState;
import ui.control.FloatSlider;
import ui.control.ParameterLabel;
import ui.parameter.FloatParameter;

/**
 *
 * @author cmolikl
 */
public class BilinearDeCasteljau extends Example {

    private class UChangeListener implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent e) {
            Point4d[][][] p = patch.evaluate2(u.getValue(), v.getValue());
            for(int k = 0; k < p.length; k++) {
                for(int i = 0; i < p[k].length; i++) {
                    for(int j = 0; j < p[k][i].length; j++) {
                        dc[k][i][j].set(p[k][i][j]);
                    }
                }
            }
            canvas.display();
        }
    }

    private class VChangeListener implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent e) {
            Point4d[][][] p = patch.evaluate2(u.getValue(), v.getValue());
            for(int k = 0; k < p.length; k++) {
                for(int i = 0; i < p[k].length; i++) {
                    for(int j = 0; j < p[k][i].length; j++) {
                        dc[k][i][j].set(p[k][i][j]);
                    }
                }
            }
            canvas.display();
        }
    }

    GLCanvas canvas;
    
    FloatParameter u = new FloatParameter("u", 0.5f);
    FloatParameter v = new FloatParameter("v", 0.5f);
    CubicBezierPatch patch;
    Point4d[][][] dc;
    
    public BilinearDeCasteljau() {
        super();
        u.addChangeListener(new UChangeListener());
        v.addChangeListener(new VChangeListener());
    }
    
    
    @Override
    public Patch[] getPatches() {
        if(patches.length == 0) {
            CubicBezierPatch patch1 = new CubicBezierPatch(new Point4d[][] {
                {new Point4d(0,-3,0,1), new Point4d(1,-3,1,1), new Point4d(2,-3,1,1), new Point4d(3,-3,0,1)},
                {new Point4d(0,-2,1,1), new Point4d(1,-2,2,1), new Point4d(2,-2,2,1), new Point4d(3,-2,1,1)},
                {new Point4d(0,-1,1,1), new Point4d(1,-1,2,1), new Point4d(2,-1,2,1), new Point4d(3,-1,1,1)},
                {new Point4d(0,0,0,1), new Point4d(1,0,1,1), new Point4d(2,0,1,1), new Point4d(3,0,0,1)}
            });
            patches = new Patch[] {patch1};
        }
        return patches;
    }

    @Override
    protected void createEffect() {

        patch = (CubicBezierPatch) getPatches()[0];

        Mesh points = new Mesh();
        points.geometry = new MeshGeometry();
        Appearance pa = new Appearance("points");
        pa.setDiffuse(1, 0, 1);
        points.appearance = pa;
        Scene pointsScene = new Scene();
        points.setRenderer(pointRenderer);
        pointsScene.addMesh(points);
        
        Scene facesScene = new Scene();

        Mesh mesh = new Mesh();
        Scene scene = new Scene();
        scene.addMesh(mesh);

        Appearance[] fa = new Appearance[4];
        fa[0] = new Appearance("faces0");
        fa[0].setDiffuse(0, 0, 1);
        fa[1] = new Appearance("faces1");
        fa[1].setDiffuse(0, 1, 0);
        fa[2] = new Appearance("faces2");
        fa[2].setDiffuse(1, 0, 0);
        fa[3] = new Appearance("faces3");
        fa[3].setDiffuse(1, 0, 1);
        dc = patch.evaluate2(u.getValue(), v.getValue());
        for(int k = 0; k < dc.length - 1; k++) {
            Mesh faces = new Mesh();
            faces.name = "faces" + k;
            faces.geometry = new MeshGeometry();
            faces.appearance = fa[k];
            for(int i = 0; i < dc[k].length -1; i ++) {
                for(int j = 0; j < dc[k][i].length - 1; j++) {
                    Vertex v0 = new Vertex(dc[k][i][j]);
                    Vertex v1 = new Vertex(dc[k][i][j+1]);
                    Vertex v2 = new Vertex(dc[k][i+1][j+1]);
                    Vertex v3 = new Vertex(dc[k][i+1][j]);
                    Face f = new Face(v0, v1, v2, v3);
                    faces.geometry.add(f);
                    faces.geometry.add(v0);
                    faces.geometry.add(v1);
                    faces.geometry.add(v2);
                    faces.geometry.add(v3);
                }
            }
            faces.setRenderer(edgeRenderer);
            facesScene.addMesh(faces);
        }
        points.getGeometry().add(new Vertex(dc[3][0][0]));
        patch.teselate(mesh, 10, smoothShading.getValue() == SMOOTH_SHADING);

        BSphere bsphere = MeshUtils.getBoundingSphere(mesh);
        scene.setBoundingSphere(bsphere);

        RenderState rs = new RenderState();
        rs.clearBuffers(true);
        rs.setClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        rs.enable(GL.GL_DEPTH_TEST);
        rs.disable(GL.GL_BLEND);

        InputStream vertexStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong2.vert");
        InputStream fragmentStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong.frag");
        Pass pass1 = new Pass(vertexStream, fragmentStream);
        pass1.scene = scene;
        pass1.renderState = rs;

        rs = new RenderState();
        rs.clearBuffers(false);

        Pass pass2 = new Pass();
        pass2.scene = facesScene;
        pass2.renderState = rs;

        Pass pass3 = new Pass();
        pass3.scene = pointsScene;
        pass3.renderState = rs;
        
        bSphereProvider = scene;

        effect = new Effect();
        effect.addGLEventListener(pass1);
        effect.addGLEventListener(pass3);
        effect.addGLEventListener(pass2);
    }
    
    @Override
    public void createUI(ExamplePanel e) {
        canvas = e.canvas;
        
        ParameterLabel uLabel = new ParameterLabel(u);
        e.params.add(uLabel);
        
        FloatSlider uSlider = new FloatSlider(u, true, JSlider.HORIZONTAL, 0f, 1f);
        e.params.add(uSlider);
        
        ParameterLabel vLabel = new ParameterLabel(v);
        e.params.add(vLabel);
        
        FloatSlider vSlider = new FloatSlider(v, true, JSlider.HORIZONTAL, 0f, 1f);
        e.params.add(vSlider);

        e.camera.center = new Vec3f(1.5f, -1.5f, 0.875f);
        e.camera.position = new Vec3f(2.248775f, -3.2315795f, 2.181367f);
        e.camera.upvector = new Vec3f(-0.22192727f, 0.5242728f, 0.82212305f);
    }

    public String getName() {
        return "Bilinear DeCasteljau";
    }


    
    public static void main(String[] args) {
        Example example = new BilinearDeCasteljau();
        PatchRenderer renderer = new PatchRenderer();
        renderer.start(example);
    }

}
