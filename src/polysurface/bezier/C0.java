/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface.bezier;

import gleem.linalg.Vec3f;
import polysurface.Example;
import polysurface.ExamplePanel;
import polysurface.Patch;

import javax.vecmath.Point4d;

/**
 *
 * @author cmolikl
 */
public class C0 extends Example {


    public Patch[] getPatches() {
        CubicBezierPatch patch1 = new CubicBezierPatch(new Point4d[][] {
                {new Point4d(0,-3,0,1), new Point4d(1,-3,1,1), new Point4d(2,-3,1,1), new Point4d(3,-3,0,1)},
                {new Point4d(0,-2,1,1), new Point4d(1,-2,2,1), new Point4d(2,-2,2,1), new Point4d(3,-2,1,1)},
                {new Point4d(0,-1,1,1), new Point4d(1,-1,2,1), new Point4d(2,-1,2,1), new Point4d(3,-1,1,1)},
                {new Point4d(0,0,0,1), new Point4d(1,0,1,1), new Point4d(2,0,1,1), new Point4d(3,0,0,1)}
        });

        CubicBezierPatch patch2 = new CubicBezierPatch(new Point4d[][] {
                {new Point4d(0,0,0,1), new Point4d(1,0,1,1), new Point4d(2,0,1,1), new Point4d(3,0,0,1)},
                {new Point4d(0,1,1,1), new Point4d(1,1,2,1), new Point4d(2,1,2,1), new Point4d(3,1,1,1)},
                {new Point4d(0,2,1,1), new Point4d(1,2,2,1), new Point4d(2,2,2,1), new Point4d(3,2,1,1)},
                {new Point4d(0,3,0,1), new Point4d(1,3,1,1), new Point4d(2,3,1,1), new Point4d(3,3,0,1)}
        });

        return new Patch[] {patch1, patch2};
    }

    public String getName() {
        return "C0 Bezier patches";
    }

    @Override
    public void createUI(ExamplePanel panel) {
        super.createUI(panel);
        panel.camera.center = new Vec3f(1.5f, 0.0f, 1.0f);
        panel.camera.position = new Vec3f(2.9798043f, 2.3160436f, 3.167056f);
        panel.camera.upvector = new Vec3f(-0.39360452f, -0.481328f, 0.7831979f);
    }
}
