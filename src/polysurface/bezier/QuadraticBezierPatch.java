/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface.bezier;

import javax.vecmath.Point4d;
import javax.vecmath.Vector3d;

import polysurface.Patch;
import scene.surface.mesh.Face;
import scene.surface.mesh.Mesh;
import scene.surface.mesh.MeshGeometry;
import scene.surface.mesh.Vertex;

/**
 *
 * @author cmolikl
 */
public class QuadraticBezierPatch implements Patch {
    public Point4d[][] cp;

    int d = 3;

    double u = 0.5;
    double v = 0.5;

    public QuadraticBezierPatch() {
        cp = new Point4d[d][d];
        for(int i = 0; i < d; i++) {
            for(int j = 0; j < d; j++) {
                cp[i][j] = new Point4d(i, j, 0, 1);
            }
        }
    }

    public QuadraticBezierPatch(Point4d[][] cp) {
        this.cp = cp;
    }

    private double signedVolumeOfTriangle(Point4d p1, Point4d p2, Point4d p3) {
        double v321 = p3.x * p2.y * p1.z;
        double v231 = p2.x * p3.y * p1.z;
        double v312 = p3.x * p1.y * p2.z;
        double v132 = p1.x * p3.y * p2.z;
        double v213 = p2.x * p1.y * p3.z;
        double v123 = p1.x * p2.y * p3.z;
        return (1.0/6.0)*(-v321 + v231 + v312 - v132 - v213 + v123);
    }


    //     0   1   2   3
    //  0  +---+---+---+
    //     |   |   |   |
    //  1  +---+---+---+
    //     |   |   |   |
    //  2  +---+---+---+
    //     |   |   |   |
    //  3  +---+---+---+

    private double getVolume() {
        double volume = 0;
        for(int i = 0; i < d-1; i++) {
            for(int j = 0; j < d-1; j++) {
                volume += signedVolumeOfTriangle(cp[i][j], cp[i+1][j], cp[i][j+1]);
                volume += signedVolumeOfTriangle(cp[i+1][j], cp[i+1][j+1], cp[i][j+1]);
            }
        }
        volume += signedVolumeOfTriangle(cp[0][0], cp[0][d-1], cp[d-1][0]);
        volume += signedVolumeOfTriangle(cp[d-1][0], cp[0][d-1], cp[d-1][d-1]);
        return Math.abs(volume);
    }

    private Point4d evaluate(double u, double v) {
        Point4d[][][] result = new Point4d[d][][];
        result[0] = cp;

        Point4d ip1 = new Point4d();
        Point4d ip2 = new Point4d();

        for(int level = 1; level < d; level++) {
            result[level] = new Point4d[d-level][d-level];
            for(int i = 0; i < d-level; i++) {
                for(int j = 0; j < d-level; j++) {
                    ip1.interpolate(result[level-1][i][j], result[level-1][i+1][j], u);
                    ip2.interpolate(result[level-1][i][j+1], result[level-1][i+1][j+1], u);
                    result[level][i][j] = li(ip1, ip2, v);
                }
            }
        }
        return result[d-1][0][0];
    }

    private Point4d li(Point4d p1, Point4d p2, double t) {
        Point4d result = new Point4d();
        result.interpolate(p1, p2, t);
        return result;
    }

    public Point4d[] deCasteljau(Point4d p0, Point4d p1, Point4d p2, double t) {
        Point4d[] result = new Point4d[5];
        result[0] = p0;
        result[4] = p2;
        result[1] = new Point4d();
        result[1].interpolate(p0, p1, t);
        result[3] = new Point4d();
        result[3].interpolate(p1, p2, t);
        result[2] = new Point4d();
        result[2].interpolate(result[1], result[3], t);
        return result;
    }

    public QuadraticBezierPatch[] subdivideU(double u) {
        Point4d[][] row = new Point4d[d][];
        for(int i = 0; i < d; i++) {
            row[i] = deCasteljau(cp[i][0], cp[i][1], cp[i][2], u);
        }

        Point4d[][] patch1 = new Point4d[][] {
            {row[0][0], row[0][1], row[0][2]},
            {row[1][0], row[1][1], row[1][2]},
            {row[2][0], row[2][1], row[2][2]}
        };

        Point4d[][] patch2 = new Point4d[][] {
            {row[0][2], row[0][3], row[0][4]},
            {row[1][2], row[1][3], row[1][4]},
            {row[2][2], row[2][3], row[2][4]}
        };

        QuadraticBezierPatch[] result = new QuadraticBezierPatch[] {
            new QuadraticBezierPatch(patch1), new QuadraticBezierPatch(patch2)
        };

        return result;
    }

    public QuadraticBezierPatch[] subdivideV(double v) {
        Point4d[][] col = new Point4d[d][];
        for(int i = 0; i < d; i++) {
            col[i] = deCasteljau(cp[0][i], cp[1][i], cp[2][i], v);
        }

        Point4d[][] patch1 = new Point4d[][] {
            {col[0][0], col[1][0], col[2][0]},
            {col[0][1], col[1][1], col[2][1]},
            {col[0][2], col[1][2], col[2][2]}
        };

        Point4d[][] patch2 = new Point4d[][] {
            {col[0][2], col[1][2], col[2][2]},
            {col[0][3], col[1][3], col[2][3]},
            {col[0][4], col[1][4], col[2][4]}
        };

        QuadraticBezierPatch[] result = new QuadraticBezierPatch[] {
            new QuadraticBezierPatch(patch1), new QuadraticBezierPatch(patch2)
        };

        return result;
    }

    public String toString() {
        String s = "";
        for(int i = 0; i < cp.length; i++) {
            for(int j = 0; j < cp[i].length; j++) {
                s += cp[j][i] + " ";
            }
            s+="\n";
        }
        return s;
    }

    private Vector3d getNormal(Point4d p1, Point4d p2, Point4d p3) {
        Vector3d n01 = new Vector3d(
            p2.x - p1.x,
            p2.y - p1.y,
            p2.z - p1.z
        );
        Vector3d n10 = new Vector3d(
            p3.x - p1.x,
            p3.y - p1.y,
            p3.z - p1.z
        );
        n01.normalize();
        n10.normalize();
        n01.cross(n01, n10);
        n01.normalize();
        return n01;
    }


    public void teselate(Mesh mesh, int res, boolean smoothShading) {
        if(mesh.geometry == null) {
            mesh.geometry = new MeshGeometry();
        }
        if(res == 0) {
            Vertex v0 = new Vertex(cp[0][0]);
            Vector3d n = getNormal(cp[0][0], cp[1][0], cp[0][1]);
            v0.setNormal(n.x, n.y, n.z);

            Vertex v1 = new Vertex(cp[d-1][0]);
            n = getNormal(cp[d-1][0], cp[d-1][1], cp[d-2][0]);
            v1.setNormal(n.x, n.y, n.z);

            Vertex v2 = new Vertex(cp[d-1][d-1]);
            n = getNormal(cp[d-1][d-1], cp[d-2][d-1], cp[d-1][d-2]);
            v2.setNormal(n.x, n.y, n.z);

            Vertex v3 = new Vertex(cp[0][d-1]);
            n = getNormal(cp[0][d-1], cp[0][d-2], cp[1][d-1]);
            v3.setNormal(n.x, n.y, n.z);

            Face f0 = new Face(v0, v1, v2);
            Face f1 = new Face(v0, v2, v3);
            mesh.geometry.add(v0);
            mesh.geometry.add(v1);
            mesh.geometry.add(v2);
            mesh.geometry.add(v3);
            mesh.geometry.add(f0);
            mesh.geometry.add(f1);
        }
        else {
            QuadraticBezierPatch[] subpatch;
            if(res % 2 == 0) subpatch = subdivideU(u);
            else subpatch = subdivideV(v);
            for(int i = 0; i < subpatch.length; i++)
                subpatch[i].teselate(mesh, res-1, smoothShading);
        }
    }

    public void getPoints(Mesh mesh) {
        for(int i = 0; i < d; i ++) {
            for(int j = 0; j < d; j++) {
               mesh.geometry.add(new Vertex(cp[i][j]));
            }
        }
    }

    public void getFaces(Mesh mesh) {
        for(int i = 0; i < d-1; i ++) {
            for(int j = 0; j < d-1; j++) {
                Vertex v0 = new Vertex(cp[i][j]);
                Vertex v1 = new Vertex(cp[i][j+1]);
                Vertex v2 = new Vertex(cp[i+1][j+1]);
                Vertex v3 = new Vertex(cp[i+1][j]);
                Face f = new Face(v0, v1, v2, v3);
                mesh.geometry.add(f);
                mesh.geometry.add(v0);
                mesh.geometry.add(v1);
                mesh.geometry.add(v2);
                mesh.geometry.add(v3);
            }
        }
    }
}
