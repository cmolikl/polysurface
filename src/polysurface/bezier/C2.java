/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface.bezier;

import gleem.linalg.Vec3f;
import polysurface.ExamplePanel;
import polysurface.Patch;
import javax.vecmath.Point4d;
import polysurface.Example;
import polysurface.PatchRenderer;

/**
 *
 * @author cmolikl
 */
public class C2 extends Example {
    
    @Override
    public Patch[] getPatches() {
        if(patches.length == 0) {
            CubicBezierPatch patch1 = new CubicBezierPatch(new Point4d[][] {
                {new Point4d(0,-3,0,1), new Point4d(1,-3,1,1), new Point4d(2,-3,1,1), new Point4d(3,-3,0,1)},
                {new Point4d(0,-2,1,1), new Point4d(1,-2,2,1), new Point4d(2,-2,2,1), new Point4d(3,-2,1,1)},
                {new Point4d(0,-1,1,1), new Point4d(1,-1,2,1), new Point4d(2,-1,2,1), new Point4d(3,-1,1,1)},
                {new Point4d(0,0,0,1), new Point4d(1,0,1,1), new Point4d(2,0,1,1), new Point4d(3,0,0,1)}
            });

            patches = patch1.subdivideU(0.5);
        }
        return patches;
    }

    public String getName() {
        return "C2 Bezier patches";
    }

    @Override
    public void createUI(ExamplePanel panel) {
        super.createUI(panel);
        panel.camera.center = new Vec3f(1.5f, -1.5f, 0.875f);
        panel.camera.position = new Vec3f(2.248775f, -3.2315795f, 2.181367f);
        panel.camera.upvector = new Vec3f(-0.22192727f, 0.5242728f, 0.82212305f);
    }

    public static void main(String[] args) {
        Example example = new C2();
        PatchRenderer renderer = new PatchRenderer();
        renderer.start(example);
    }
}
