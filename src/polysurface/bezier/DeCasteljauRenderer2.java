/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface.bezier;

import gleem.BSphere;
import java.awt.BorderLayout;
import java.io.InputStream;
import javax.media.opengl.GL;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Point4d;
import scene.surface.Appearance;
import scene.surface.mesh.Face;
import scene.surface.mesh.Mesh;
import scene.surface.mesh.MeshGeometry;
import scene.surface.mesh.MeshUtils;
import scene.Scene;
import scene.surface.mesh.Vertex;
import tiger.core.Effect;
import tiger.core.Pass;
import tiger.core.RenderState;
import tiger.core.Window;
import tiger.example.EdgeMeshRenderer;
import tiger.example.PointMeshRenderer;

/**
 *
 * @author cmolikl
 */
public class DeCasteljauRenderer2 {

    EdgeMeshRenderer edgeRenderer = new EdgeMeshRenderer();
    PointMeshRenderer pointRenderer = new PointMeshRenderer();

    private class UChangeListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            JSlider s = (JSlider)e.getSource();
            u = s.getValue() / 100.0;
            for(int k = 0; k < 4; k++) {
                Point4d[] ldc = patch.deCasteljau(patch.cp[k][0], patch.cp[k][1], patch.cp[k][2], patch.cp[k][3], u);
                for(int i = 0; i < ldc.length; i++) {
                    dc[k][i].set(ldc[i]);
                }
            }   
            canvas.display();
        }
    }

    private class VChangeListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            JSlider s = (JSlider)e.getSource();
            v = s.getValue() / 100.0;
            
            canvas.display();
        }
    }

    GLCanvas canvas;
    
    double u = 0.5;
    double v = 0.5;
    CubicBezierPatch patch;
    Point4d[][] dc;

    public void start(CubicBezierPatch patch) {

        this.patch = patch;

        Mesh points = new Mesh();
        points.geometry = new MeshGeometry();
        Appearance pa = new Appearance("points");
        pa.setDiffuse(1, 0, 1);
        points.appearance = pa;
        Scene pointsScene = new Scene();
        points.setRenderer(pointRenderer);
        pointsScene.addMesh(points);

        //Mesh points1 = new Mesh();
        /*points1.geometry = new MeshGeometry();
        Appearance pa1 = new Appearance("points1");
        pa1.setDiffuse(0, 0, 1);
        points1.appearance = pa1;
        Scene pointsScene1 = new Scene();
        pointsScene1.addMesh(points1);*/


        
        Scene facesScene = new Scene();

        Mesh mesh = new Mesh();
        Scene scene = new Scene();
        scene.addMesh(mesh);

        //patch.getPoints(points1);

        Appearance[] fa = new Appearance[5];
        fa[0] = new Appearance("faces0");
        fa[0].setDiffuse(0, 0, 1);
        fa[1] = new Appearance("faces1");
        fa[1].setDiffuse(0, 1, 0);
        fa[2] = new Appearance("faces2");
        fa[2].setDiffuse(1, 0, 0);
        fa[3] = new Appearance("faces3");
        fa[3].setDiffuse(1, 0, 1);
        fa[4] = new Appearance("faces4");
        fa[4].setDiffuse(1, 1, 0);
        dc = new Point4d[4][];
        for(int k = 0; k < 4; k++) {
            dc[k] = patch.deCasteljau(patch.cp[k][0], patch.cp[k][1], patch.cp[k][2], patch.cp[k][3], u);
        }
        //dc[4] = patch.deCasteljau(dc[0][3], dc[1][3], dc[2][3], dc[3][3], v);

        for(int k = 0; k < dc.length; k++) {
            Mesh faces = new Mesh();
            faces.name = "faces" + k;
            faces.geometry = new MeshGeometry();
            faces.appearance = fa[k];
            Vertex[] v = new Vertex[dc[k].length + 1];
            for(int i = 0; i < dc[k].length; i ++) {
               v[i] = new Vertex(dc[k][i]);
               faces.geometry.add(v[i]);
            }
            Point4d p = new Point4d();
            p.interpolate(patch.cp[k][1], patch.cp[k][2], u);
            v[dc[k].length] = new Vertex(p);

            Face f = new Face(v[1], v[2], v[2]);
            faces.geometry.add(f);
            f = new Face(v[2], v[dc[k].length], v[dc[k].length]);
            faces.geometry.add(f);
            f = new Face(v[dc[k].length], v[5], v[5]);
            faces.geometry.add(f);
            f = new Face(v[2], v[4], v[4]);
            faces.geometry.add(f);
            faces.setRenderer(edgeRenderer);
            facesScene.addMesh(faces);
        }
        //points.getGeometry().add(new Vertex(dc[4][0]));
        patch.teselate(mesh, 10, true);

        BSphere bsphere = MeshUtils.getBoundingSphere(mesh);
        scene.setBoundingSphere(bsphere);

        RenderState rs = new RenderState();
        rs.clearBuffers(true);
        rs.setClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        rs.enable(GL.GL_DEPTH_TEST);
        rs.disable(GL.GL_BLEND);

        InputStream vertexStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong2.vert");
        InputStream fragmentStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong.frag");
        Pass pass1 = new Pass(vertexStream, fragmentStream);
        pass1.scene = scene;
        pass1.renderState = rs;

        rs = new RenderState();
        rs.clearBuffers(false);

        Pass pass2 = new Pass();
        pass2.scene = facesScene;
        pass2.renderState = rs;

        Pass pass3 = new Pass();
        pass3.scene = pointsScene;
        pass3.renderState = rs;

        /*Pass pass4 = new PointRenderPass();
        pass4.scene = pointsScene1;
        pass4.renderState = rs;*/

        Effect effect = new Effect();
        effect.addGLEventListener(pass1);
        effect.addGLEventListener(pass3);
        //effect.addGLEventListener(pass4);
        effect.addGLEventListener(pass2);

        Window w = new Window(scene, 512, 512);
        w.setEffect(effect);
        w.build();

        canvas = w.canvas;

        JSlider uParam = new JSlider(JSlider.VERTICAL, 0, 100, 50);
        uParam.addChangeListener(new UChangeListener());
        w.frame.getContentPane().add(uParam, BorderLayout.EAST);

        JSlider vParam = new JSlider(JSlider.HORIZONTAL, 0, 100, 50);
        vParam.addChangeListener(new VChangeListener());
        w.frame.getContentPane().add(vParam, BorderLayout.SOUTH);

        w.run();
    }

    public static void main(String[] args) {

        CubicBezierPatch patch1 = new CubicBezierPatch(new Point4d[][] {
            {new Point4d(0,-3,0,1), new Point4d(1,-3,1,1), new Point4d(2,-3,1,1), new Point4d(3,-3,0,1)},
            {new Point4d(0,-2,1,1), new Point4d(1,-2,2,1), new Point4d(2,-2,2,1), new Point4d(3,-2,1,1)},
            {new Point4d(0,-1,1,1), new Point4d(1,-1,2,1), new Point4d(2,-1,2,1), new Point4d(3,-1,1,1)},
            {new Point4d(0,0,0,1), new Point4d(1,0,1,1), new Point4d(2,0,1,1), new Point4d(3,0,0,1)}
        });

        DeCasteljauRenderer2 renderer = new DeCasteljauRenderer2();
        renderer.start(patch1);
    }

}
