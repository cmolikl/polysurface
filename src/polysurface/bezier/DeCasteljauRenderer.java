/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface.bezier;

import gleem.BSphere;
import java.awt.BorderLayout;
import java.io.InputStream;
import javax.media.opengl.GL;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Point4d;
import scene.surface.Appearance;
import scene.surface.mesh.Mesh;
import scene.surface.mesh.MeshGeometry;
import scene.surface.mesh.MeshUtils;
import scene.Scene;
import scene.surface.mesh.Vertex;
import tiger.core.Effect;
import tiger.core.Pass;
import tiger.core.RenderState;
import tiger.core.Window;
import tiger.example.EdgeMeshRenderer;
import tiger.example.PointMeshRenderer;

/**
 *
 * @author cmolikl
 */
public class DeCasteljauRenderer {

    EdgeMeshRenderer edgeRenderer = new EdgeMeshRenderer();
    PointMeshRenderer pointRenderer = new PointMeshRenderer();

    private class UChangeListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            JSlider s = (JSlider)e.getSource();
            u = s.getValue() / 100.0;
            Point4d p = patch.evaluate(u, v);
            Point4d loc = points.geometry.getVertexList().get(0).getLocation();
            loc.set(p);
            canvas.display();
        }
    }

    private class VChangeListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            JSlider s = (JSlider)e.getSource();
            v = s.getValue() / 100.0;
            Point4d p = patch.evaluate(u, v);
            Point4d loc = points.geometry.getVertexList().get(0).getLocation();
            loc.set(p);
            canvas.display();
        }
    }

    GLCanvas canvas;
    
    double u = 0.5;
    double v = 0.5;
    CubicBezierPatch patch;
    Mesh points;

    public void start(CubicBezierPatch patch) {

        this.patch = patch;

        points = new Mesh();
        points.geometry = new MeshGeometry();
        Appearance pa = new Appearance("points");
        pa.setDiffuse(1, 0, 0);
        points.appearance = pa;
        Scene pointsScene = new Scene();
        points.setRenderer(pointRenderer);
        pointsScene.addMesh(points);

        Mesh points1 = new Mesh();
        points1.geometry = new MeshGeometry();
        Appearance pa1 = new Appearance("points1");
        pa1.setDiffuse(0, 0, 1);
        points1.appearance = pa1;
        Scene pointsScene1 = new Scene();
        points1.setRenderer(pointRenderer);
        pointsScene1.addMesh(points1);


        Mesh faces = new Mesh();
        faces.geometry = new MeshGeometry();
        Appearance fa = new Appearance("faces");
        fa.setDiffuse(0, 0, 1);
        faces.appearance = fa;
        Scene facesScene = new Scene();
        faces.setRenderer(edgeRenderer);
        facesScene.addMesh(faces);

        Mesh mesh = new Mesh();
        Scene scene = new Scene();
        scene.addMesh(mesh);

        //patch.getPoints(points);
        Point4d p = patch.evaluate(u, u);
        points.getGeometry().add(new Vertex(p));
        patch.getPoints(points1);
        patch.getFaces(faces);
        patch.teselate(mesh, 10, true);

        BSphere bsphere = MeshUtils.getBoundingSphere(mesh);
        scene.setBoundingSphere(bsphere);

        RenderState rs = new RenderState();
        rs.clearBuffers(true);
        rs.setClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        rs.enable(GL.GL_DEPTH_TEST);
        rs.disable(GL.GL_BLEND);

        InputStream vertexStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong2.vert");
        InputStream fragmentStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong.frag");
        Pass pass1 = new Pass(vertexStream, fragmentStream);
        pass1.scene = scene;
        pass1.renderState = rs;

        rs = new RenderState();
        rs.clearBuffers(false);

        Pass pass2 = new Pass();
        pass2.scene = facesScene;
        pass2.renderState = rs;

        Pass pass3 = new Pass();
        pass3.scene = pointsScene;
        pass3.renderState = rs;

        Pass pass4 = new Pass();
        pass4.scene = pointsScene1;
        pass4.renderState = rs;

        Effect effect = new Effect();
        effect.addGLEventListener(pass1);
        effect.addGLEventListener(pass3);
        effect.addGLEventListener(pass4);
        effect.addGLEventListener(pass2);

        Window w = new Window(scene, 512, 512);
        w.setEffect(effect);
        w.build();

        canvas = w.canvas;

        JSlider uParam = new JSlider(JSlider.VERTICAL, 0, 100, 50);
        uParam.addChangeListener(new UChangeListener());
        w.frame.getContentPane().add(uParam, BorderLayout.EAST);

        JSlider vParam = new JSlider(JSlider.HORIZONTAL, 0, 100, 50);
        vParam.addChangeListener(new VChangeListener());
        w.frame.getContentPane().add(vParam, BorderLayout.SOUTH);

        w.run();
    }

    public static void main(String[] args) {

        CubicBezierPatch patch1 = new CubicBezierPatch(new Point4d[][] {
            {new Point4d(0,-3,0,1), new Point4d(1,-3,1,1), new Point4d(2,-3,1,1), new Point4d(3,-3,0,1)},
            {new Point4d(0,-2,1,1), new Point4d(1,-2,2,1), new Point4d(2,-2,2,1), new Point4d(3,-2,1,1)},
            {new Point4d(0,-1,1,1), new Point4d(1,-1,2,1), new Point4d(2,-1,2,1), new Point4d(3,-1,1,1)},
            {new Point4d(0,0,0,1), new Point4d(1,0,1,1), new Point4d(2,0,1,1), new Point4d(3,0,0,1)}
        });

        DeCasteljauRenderer renderer = new DeCasteljauRenderer();
        renderer.start(patch1);
    }

}
