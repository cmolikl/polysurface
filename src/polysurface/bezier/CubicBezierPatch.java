/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package polysurface.bezier;

import gleem.BSphere;
import java.io.InputStream;
import javax.media.opengl.GL;
import javax.vecmath.Point4d;
import javax.vecmath.Vector3d;

import polysurface.Patch;
import scene.surface.Appearance;
import scene.surface.mesh.Face;
import scene.surface.mesh.Mesh;
import scene.surface.mesh.MeshGeometry;
import scene.surface.mesh.MeshUtils;
import scene.Scene;
import scene.surface.mesh.Vertex;
import tiger.core.Effect;
import tiger.core.Pass;
import tiger.core.RenderState;
import tiger.core.Window;
import tiger.example.EdgeMeshRenderer;
import tiger.example.PointMeshRenderer;
import ui.parameter.IntParameter;

/**
 *
 * @author cmolikl
 */
public class CubicBezierPatch implements Patch {
    public Point4d[][] cp;

    double u = 0.5;
    double v = 0.5;

    static EdgeMeshRenderer edgeRenderer = new EdgeMeshRenderer();
    static PointMeshRenderer pointRenderer = new PointMeshRenderer();

    public CubicBezierPatch() {
        cp = new Point4d[4][4];
        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 4; j++) {
                cp[i][j] = new Point4d(i, j, 0, 1);
            }
        }
    }

    public CubicBezierPatch(Point4d[][] cp) {
        this.cp = cp;
    }

    private double signedVolumeOfTriangle(Point4d p1, Point4d p2, Point4d p3) {
        double v321 = p3.x * p2.y * p1.z;
        double v231 = p2.x * p3.y * p1.z;
        double v312 = p3.x * p1.y * p2.z;
        double v132 = p1.x * p3.y * p2.z;
        double v213 = p2.x * p1.y * p3.z;
        double v123 = p1.x * p2.y * p3.z;
        return (1.0/6.0)*(-v321 + v231 + v312 - v132 - v213 + v123);
    }


    //     0   1   2   3
    //  0  +---+---+---+
    //     |   |   |   |
    //  1  +---+---+---+
    //     |   |   |   |
    //  2  +---+---+---+
    //     |   |   |   |
    //  3  +---+---+---+

    private double getVolume() {
        double volume = 0;
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                volume += signedVolumeOfTriangle(cp[i][j], cp[i+1][j], cp[i][j+1]);
                volume += signedVolumeOfTriangle(cp[i+1][j], cp[i+1][j+1], cp[i][j+1]);
            }
        }
        volume += signedVolumeOfTriangle(cp[0][0], cp[0][3], cp[3][0]);
        volume += signedVolumeOfTriangle(cp[3][0], cp[0][3], cp[3][3]);
        return Math.abs(volume);
    }

    public Point4d evaluate(double u, double v) {
        return evaluate2(u, v)[3][0][0];
    }

    public Point4d[][][] evaluate2(double u, double v) {
        Point4d[][][] result = new Point4d[4][][];
        result[0] = cp;

        Point4d ip1 = new Point4d();
        Point4d ip2 = new Point4d();

        for(int level = 1; level < 4; level++) {
            result[level] = new Point4d[4-level][4-level];
            for(int i = 0; i < 4-level; i++) {
                for(int j = 0; j < 4-level; j++) {
                    ip1.interpolate(result[level-1][i][j], result[level-1][i+1][j], u);
                    ip2.interpolate(result[level-1][i][j+1], result[level-1][i+1][j+1], u);
                    result[level][i][j] = li(ip1, ip2, v);
                }
            }
        }
        return result;
    }

    private Point4d li(Point4d p1, Point4d p2, double t) {
        Point4d result = new Point4d();
        result.interpolate(p1, p2, t);
        return result;
    }

    public Point4d[] deCasteljau(Point4d p0, Point4d p1, Point4d p2, Point4d p3, double t) {
        Point4d[] result = new Point4d[7];
        result[0] = p0;
        result[6] = p3;
        result[1] = new Point4d();
        result[1].interpolate(p0, p1, t);
        result[5] = new Point4d();
        result[5].interpolate(p2, p3, t);
        Point4d p12 = new Point4d();
        p12.interpolate(p1, p2, t);
        result[2] = new Point4d();
        result[2].interpolate(result[1], p12, t);
        result[4] = new Point4d();
        result[4].interpolate(p12, result[5], t);
        result[3] = new Point4d();
        result[3].interpolate(result[2], result[4], t);
        return result;
    }

    public Point4d[][] deCasteljau2(Point4d[] p, double t) {
        Point4d[][] result = new Point4d[p.length][];
        result[0] = p;
        for(int k = 1; k < p.length; k++) {
            result[k] = new Point4d[p.length - k];
            for(int i = 0; i < p.length-k; i++) {
                result[k][i] = li(result[k-1][i], result[k-1][i+1], t);
            }
        }
        return result;
    }

    public CubicBezierPatch[] subdivideU(double u) {
        Point4d[][] row = new Point4d[4][];
        for(int i = 0; i < 4; i++) {
            row[i] = deCasteljau(cp[i][0], cp[i][1], cp[i][2], cp[i][3], u);
        }

        Point4d[][] patch1 = new Point4d[][] {
            {row[0][0], row[0][1], row[0][2], row[0][3]},
            {row[1][0], row[1][1], row[1][2], row[1][3]},
            {row[2][0], row[2][1], row[2][2], row[2][3]},
            {row[3][0], row[3][1], row[3][2], row[3][3]}
        };

        Point4d[][] patch2 = new Point4d[][] {
            {row[0][3], row[0][4], row[0][5], row[0][6]},
            {row[1][3], row[1][4], row[1][5], row[1][6]},
            {row[2][3], row[2][4], row[2][5], row[2][6]},
            {row[3][3], row[3][4], row[3][5], row[3][6]}
        };

        CubicBezierPatch[] result = new CubicBezierPatch[] {
            new CubicBezierPatch(patch1), new CubicBezierPatch(patch2)
        };

        return result;
    }

    public CubicBezierPatch[] subdivideV(double v) {
        Point4d[][] col = new Point4d[4][];
        for(int i = 0; i < 4; i++) {
            col[i] = deCasteljau(cp[0][i], cp[1][i], cp[2][i], cp[3][i], v);
        }

        Point4d[][] patch1 = new Point4d[][] {
            {col[0][0], col[1][0], col[2][0], col[3][0]},
            {col[0][1], col[1][1], col[2][1], col[3][1]},
            {col[0][2], col[1][2], col[2][2], col[3][2]},
            {col[0][3], col[1][3], col[2][3], col[3][3]}
        };

        Point4d[][] patch2 = new Point4d[][] {
            {col[0][3], col[1][3], col[2][3], col[3][3]},
            {col[0][4], col[1][4], col[2][4], col[3][4]},
            {col[0][5], col[1][5], col[2][5], col[3][5]},
            {col[0][6], col[1][6], col[2][6], col[3][6]}
        };

        CubicBezierPatch[] result = new CubicBezierPatch[] {
            new CubicBezierPatch(patch1), new CubicBezierPatch(patch2)
        };

        return result;
    }

    public String toString() {
        String s = "";
        for(int i = 0; i < cp.length; i++) {
            for(int j = 0; j < cp[i].length; j++) {
                s += cp[j][i] + " ";
            }
            s+="\n";
        }
        return s;
    }

    private Vector3d getNormal(Point4d p1, Point4d p2, Point4d p3) {
        Vector3d n01 = new Vector3d(
            p2.x - p1.x,
            p2.y - p1.y,
            p2.z - p1.z
        );
        Vector3d n10 = new Vector3d(
            p3.x - p1.x,
            p3.y - p1.y,
            p3.z - p1.z
        );
        n01.normalize();
        n10.normalize();
        n01.cross(n01, n10);
        n01.normalize();
        return n01;
    }

    public void teselate(Mesh mesh, int res, boolean smoothShading) {
        if(smoothShading) {
            teselateSmooth(mesh, res);
        }
        else {
            teselateFlat(mesh, res);
        }
    }


    public void teselateSmooth(Mesh mesh, int res) {
        if(mesh.geometry == null) {
            mesh.geometry = new MeshGeometry();
        }
        if(res == 0) {
            Vertex v0 = new Vertex(cp[0][0]);
            Vertex v1 = new Vertex(cp[3][0]);
            Vertex v2 = new Vertex(cp[3][3]);
            Vertex v3 = new Vertex(cp[0][3]);

            Vector3d n = getNormal(cp[0][0], cp[1][0], cp[0][1]);
            v0.setNormal(n.x, n.y, n.z);
            n = getNormal(cp[3][0], cp[3][1], cp[2][0]);
            v1.setNormal(n.x, n.y, n.z);
            n = getNormal(cp[3][3], cp[2][3], cp[3][2]);
            v2.setNormal(n.x, n.y, n.z);
            n = getNormal(cp[0][3], cp[0][2], cp[1][3]);
            v3.setNormal(n.x, n.y, n.z);

            Face f0 = new Face(v0, v1, v2);
            Face f1 = new Face(v0, v2, v3);
            mesh.geometry.add(v0);
            mesh.geometry.add(v1);
            mesh.geometry.add(v2);
            mesh.geometry.add(v3);
            mesh.geometry.add(f0);
            mesh.geometry.add(f1);
        }
        else {
            CubicBezierPatch[] subpatch;
            if(res % 2 == 0) subpatch = subdivideU(u);
            else subpatch = subdivideV(v);
            for(int i = 0; i < subpatch.length; i++)
                subpatch[i].teselateSmooth(mesh, res-1);
        }
    }

    public void teselateFlat(Mesh mesh, int res) {
        if(mesh.geometry == null) {
            mesh.geometry = new MeshGeometry();
        }
        if(res == 0) {
            Vertex v0 = new Vertex(cp[0][0]);
            Vertex v1 = new Vertex(cp[3][0]);
            Vertex v2 = new Vertex(cp[3][3]);
            Vertex v3 = new Vertex(cp[0][3]);

            Vector3d n = getNormal(v0.getLocation(), v1.getLocation(), v3.getLocation());
            v0.setNormal(n.x, n.y, n.z);
            v1.setNormal(n.x, n.y, n.z);
            v2.setNormal(n.x, n.y, n.z);
            v3.setNormal(n.x, n.y, n.z);

            Face f0 = new Face(v0, v1, v2);
            Face f1 = new Face(v0, v2, v3);
            mesh.geometry.add(v0);
            mesh.geometry.add(v1);
            mesh.geometry.add(v2);
            mesh.geometry.add(v3);
            mesh.geometry.add(f0);
            mesh.geometry.add(f1);
        }
        else {
            CubicBezierPatch[] subpatch;
            if(res % 2 == 0) subpatch = subdivideU(u);
            else subpatch = subdivideV(v);
            for(int i = 0; i < subpatch.length; i++)
                subpatch[i].teselateFlat(mesh, res-1);
        }
    }

    public void getPoints(Mesh mesh) {
        for(int i = 0; i < 4; i ++) {
            for(int j = 0; j < 4; j++) {
               mesh.geometry.add(new Vertex(cp[i][j]));
            }
        }
    }

    public void getFaces(Mesh mesh) {
        for(int i = 0; i < 3; i ++) {
            for(int j = 0; j < 3; j++) {
                Vertex v0 = new Vertex(cp[i][j]);
                Vertex v1 = new Vertex(cp[i][j+1]);
                Vertex v2 = new Vertex(cp[i+1][j+1]);
                Vertex v3 = new Vertex(cp[i+1][j]);
                Face f = new Face(v0, v1, v2, v3);
                mesh.geometry.add(f);
                mesh.geometry.add(v0);
                mesh.geometry.add(v1);
                mesh.geometry.add(v2);
                mesh.geometry.add(v3);
            }
        }
    }

    public static void main(String[] args) {
        CubicBezierPatch patch = new CubicBezierPatch();
        patch.cp[1][1].z = 2;
        patch.cp[1][2].z = 2;
        patch.cp[2][1].z = 2;
        patch.cp[2][2].z = 2;

        Mesh points1 = new Mesh();
        points1.geometry = new MeshGeometry();
        for(int i = 0; i < 4; i ++) {
            for(int j = 0; j < 4; j++) {
                points1.geometry.add(new Vertex(patch.cp[i][j]));
            }
        }
        Appearance pa1 = new Appearance("points1");
        pa1.setDiffuse(0, 1, 0);
        points1.appearance = pa1;
        Scene pointsScene1 = new Scene();
        points1.setRenderer(pointRenderer);
        pointsScene1.addMesh(points1);

        Mesh faces = new Mesh();
        faces.geometry = new MeshGeometry();
        for(int i = 0; i < 3; i ++) {
            for(int j = 0; j < 3; j++) {
                Vertex v0 = new Vertex(patch.cp[i][j]);
                Vertex v1 = new Vertex(patch.cp[i][j+1]);
                Vertex v2 = new Vertex(patch.cp[i+1][j+1]);
                Vertex v3 = new Vertex(patch.cp[i+1][j]);
                Face f = new Face(v0, v1, v2, v3);
                faces.geometry.add(f);
                faces.geometry.add(v0);
                faces.geometry.add(v1);
                faces.geometry.add(v2);
                faces.geometry.add(v3);
            }
        }
        Appearance fa = new Appearance("faces");
        fa.setDiffuse(0, 1, 0);
        faces.appearance = fa;
        Scene facesScene = new Scene();
        faces.setRenderer(edgeRenderer);
        facesScene.addMesh(faces);


        Mesh mesh = new Mesh();
        patch.teselate(mesh, 10, true);
        Scene scene = new Scene();
        scene.addMesh(mesh);
        BSphere bsphere = MeshUtils.getBoundingSphere(mesh);
        scene.setBoundingSphere(bsphere);

        RenderState rs = new RenderState();
        rs.clearBuffers(true);
        rs.enable(GL.GL_DEPTH_TEST);

        InputStream vertexStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong2.vert");
        InputStream fragmentStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong.frag");
        Pass pass1 = new Pass(vertexStream, fragmentStream);
        pass1.scene = scene;
        pass1.renderState = rs;
        /*pass1.setTarget(fbo);

        rs = new RenderState();
        rs.clearBuffers(true);
        rs.disable(GL.GL_DEPTH_TEST);*/

        //pass2.renderState = rs;
        Pass pass2 = new Pass();
        pass2.scene = facesScene;

        Pass pass3 = new Pass();
        pass3.scene = pointsScene1;

        Effect effect = new Effect();
        effect.addGLEventListener(pass1);
        effect.addGLEventListener(pass3);
        effect.addGLEventListener(pass2);
        //effect.addTexture(texture);
        //effect.addTarget(fbo);

        Window w = new Window(scene, 512, 512);
        w.setEffect(effect);
        w.runFastAsPosible = true;
        w.start();
    }

}
