package polysurface.bezier;

import gleem.BSphereProvider;
import polysurface.Example;
import polysurface.ExamplePanel;
import polysurface.Patch;
import scene.surface.mesh.Mesh;
import tiger.core.Effect;
import ui.control.OneOfN;
import ui.control.OneOfNGroup;
import ui.control.OneOfNMenuItem;
import ui.parameter.IntParameter;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Point3f;

public class CubicBezierPatchExample extends Example {

    IntParameter continuity;
    Example c0Example, c1Example, c2Example;

    public CubicBezierPatchExample() {
        continuity = new IntParameter("Continuity", 0);

        c0Example = new C0();
        c1Example = new C1();
        c2Example = new C2();
    }

    @Override
    public Patch[] getPatches() {
        Patch[] result;
        switch(continuity.getValue()) {
            case 1:
                result = c1Example.getPatches();
                break;
            case 2:
                result = c2Example.getPatches();
                break;
            default:
            case 0:
                result = c0Example.getPatches();
                break;
        }
        return result;
    }

    @Override
    public BSphereProvider getBSphereProvider() {
        switch(continuity.getValue()) {
            case 1:
                bSphereProvider = c1Example.getBSphereProvider();
                break;
            case 2:
                bSphereProvider = c2Example.getBSphereProvider();
                break;
            default:
            case 0:
                bSphereProvider = c0Example.getBSphereProvider();
                break;
        }
        return bSphereProvider;
    }

    public void createUI(ExamplePanel panel) {
        ChangeListener continuityListener = new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                OneOfNMenuItem item = (OneOfNMenuItem) e.getSource();
                IntParameter ip = item.getParam();
                if(ip.getValue() == 1) {
                    if("C2".equals(ip.name)) {
                        continuity.setValue(2);
                        //copyScenes(c2Example);
                    }
                    else if("C1".equals(ip.name)) {
                        continuity.setValue(1);
                        //copyScenes(c1Example);
                    }
                    else if("C0".equals(ip.name)) {
                        continuity.setValue(0);
                        //copyScenes(c0Example);
                    }
                    subdivide();
                    getBSphereProvider();
                    panel.camera.viewAll();
                    panel.canvas.repaint();
                }
            }
        };

        OneOfNGroup group = new OneOfNGroup(panel.params, 3, continuityListener, new String[] {"C0", "C1", "C2"});
    }

    private void copyScenes(Example example) {
        pointsScene.clear();
        for(Mesh mesh : example.pointsScene.getAllMeshes()) {
            pointsScene.addMesh(mesh);
        }
        edgesScene.clear();
        for(Mesh mesh : example.edgesScene.getAllMeshes()) {
            edgesScene.addMesh(mesh);
        }
        scene.clear();
        for(Mesh mesh : example.scene.getAllMeshes()) {
            scene.addMesh(mesh);
        }
    }
}
