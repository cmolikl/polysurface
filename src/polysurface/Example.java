package polysurface;

import gleem.BSphere;
import gleem.BSphereProvider;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;
import javax.media.opengl.GL;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import polysurface.subdiv.CatmullClarkSurface;
import scene.Scene;
import scene.surface.Appearance;
import scene.surface.mesh.Mesh;
import scene.surface.mesh.MeshGeometry;
import scene.surface.mesh.MeshUtils;
import tiger.core.Effect;
import tiger.core.Pass;
import tiger.core.RenderState;
import tiger.core.Window;
import tiger.example.EdgeMeshRenderer;
import tiger.example.PointMeshRenderer;
import ui.control.BooleanCheckBox;
import ui.control.BooleanMenuItem;
import ui.control.IntSlider;
import ui.control.ParameterLabel;
import ui.parameter.IntParameter;

public abstract class Example {
    
    protected static final EdgeMeshRenderer edgeRenderer = new EdgeMeshRenderer();
    protected static final PointMeshRenderer pointRenderer = new PointMeshRenderer();
    
    public Patch[] patches = new Patch[] {};
    public Effect effect;
    public BSphereProvider bSphereProvider;

    public static int FLAT_SHADING = 0;
    public static int SMOOTH_SHADING = 1;
    protected IntParameter smoothShading = new IntParameter("Smooth shading", SMOOTH_SHADING);
    protected IntParameter subdivisions = new IntParameter("Number of subdivisions", 4);
    public Scene<Mesh> pointsScene;
    public Scene<Mesh> edgesScene;
    public Scene<Mesh> scene;
    
    public Patch[] getPatches() {
        return patches;
    }

    public void subdivide() {
        Patch[] patches = getPatches();

        Appearance pa = new Appearance("points");
        pa.setDiffuse(0, 0, 0);

        Mesh points = new Mesh();
        points.geometry = new MeshGeometry();
        points.appearance = pa;
        points.setRenderer(pointRenderer);

        if (pointsScene == null) {
            pointsScene = new Scene();
        }
        else {
            pointsScene.clear();
        }
        pointsScene.addMesh(points);

        Appearance[] fa = new Appearance[4];
        fa[0] = new Appearance("faces0");
        fa[0].setDiffuse(0, 0, 1);
        fa[1] = new Appearance("faces1");
        fa[1].setDiffuse(0, 1, 0);
        fa[2] = new Appearance("faces2");
        fa[2].setDiffuse(1, 0, 0);
        fa[3] = new Appearance("faces3");
        fa[3].setDiffuse(1, 0, 1);

        if (edgesScene == null) {
            edgesScene = new Scene();
        }
        else {
            edgesScene.clear();
        }
        Mesh[] faces = new Mesh[patches.length];
        for(int i = 0; i < patches.length; i++) {
            faces[i] = new Mesh();
            faces[i].name = "faces" + i;
            faces[i].geometry = new MeshGeometry();
            faces[i].appearance = fa[i%4];
            faces[i].setRenderer(edgeRenderer);
            edgesScene.addMesh(faces[i]);
        }

        Mesh mesh = new Mesh();
        if (scene == null) {
            scene = new Scene();
        }
        else {
            scene.clear();
        }
        scene.addMesh(mesh);

        int i = 0;
        for(Patch patch : patches) {
            if(smoothShading.getValue() == SMOOTH_SHADING) {
                patch.teselate(mesh, 2 * subdivisions.getValue(), true);
            }
            else {
                patch.teselate(mesh, 2 * subdivisions.getValue(), false);
            }
            patch.getPoints(points);
            patch.getFaces(faces[i]);
            i++;
        }

        pointsScene.setBoundingSphere(MeshUtils.getBoundingSphere(points));
        bSphereProvider = pointsScene;
    }
    
    protected void createEffect() {

        subdivide();

        RenderState rs = new RenderState();
        rs.clearBuffers(true);
        rs.setClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        rs.enable(GL.GL_DEPTH_TEST);
        rs.disable(GL.GL_BLEND);

        InputStream vertexStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong2.vert");
        InputStream fragmentStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong.frag");
        Pass pass1 = new Pass(vertexStream, fragmentStream);
        pass1.scene = scene;
        pass1.renderState = rs;

        RenderState rs2 = new RenderState();
        rs2.clearBuffers(false);

        Pass pass2 = new Pass();
        pass2.scene = edgesScene;
        pass2.renderState = rs2;

        Pass pass3 = new Pass();
        pass3.scene = pointsScene;
        pass3.renderState = rs2;

        effect = new Effect();
        effect.addGLEventListener(pass1);
        effect.addGLEventListener(pass2);
        effect.addGLEventListener(pass3);
    }
    
    public Effect getEffect() {
        if(effect == null) {
            createEffect();
        }
        return effect;
    }
    
    public BSphereProvider getBSphereProvider() {
        if(bSphereProvider == null) {
            createEffect();
        }
        return bSphereProvider;
    }

    public void createUI(ExamplePanel panel) {
        ChangeListener changeListener = new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                subdivide();
                panel.canvas.repaint();
            }
        };

        subdivisions.addChangeListener(changeListener);
        smoothShading.addChangeListener(changeListener);

//        JButton cameraButton = new JButton("Print camera params");
//        cameraButton.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                System.out.println("Camera center: " + panel.camera.center);
//                System.out.println("Camera position: " + panel.camera.position);
//                System.out.println("Camera upvector: " + panel.camera.upvector);
//                System.out.println("Camera scale: " + panel.camera.scale);
//            }
//        });
//
//        panel.params.add(cameraButton);
        panel.params.add(new ParameterLabel(subdivisions));
        panel.params.add(new IntSlider(subdivisions, JSlider.HORIZONTAL, 1, 4));
        panel.params.add(new ParameterLabel(smoothShading));
        panel.params.add(new BooleanCheckBox("SmoothShading", smoothShading));
    }

    public String getName() {
        return "Example";
    }
}
