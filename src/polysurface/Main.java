package polysurface;

import polysurface.bezier.*;
import polysurface.bspline.CubicBSplineExample;
import polysurface.bspline.DeBoorRenderer;
import polysurface.subdiv.SubdivisionSurfaceExample;

import javax.swing.*;
import java.io.*;
import java.net.URL;

public class Main {
    public static void main(String... args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.err.println("The atempt to set system Look&Feel failed. Continuing with default.");
        }

        String aboutText = "Error while loading the text.";
        try {
            InputStream aboutStream = ClassLoader.getSystemResourceAsStream("about.html");
            BufferedReader reader = new BufferedReader(new InputStreamReader(aboutStream));
            String line = reader.readLine();
            if(line != null) {
                aboutText = "";
            }
            while(line != null) {
                aboutText += line + "\n";
                line = reader.readLine();
            }
        }
        catch(FileNotFoundException e) {
            e.printStackTrace();
        }
        catch(IOException e) {
            e.printStackTrace();
        }

        JEditorPane editorPane = new JEditorPane();
        editorPane.setContentType("text/html");
        editorPane.setText(aboutText);
        editorPane.setEditable(false);

        JScrollPane about = new JScrollPane(editorPane);

        Example[] examples = new Example[] {
                new C0(),
                new C1(),
                //new C1Corner(),
                new C2(),
                //new CubicBezierPatchExample(),
                new BilinearDeCasteljau(),
                new CubicBSplineExample(),
                new DeBoorRenderer(),
                new SubdivisionSurfaceExample()
        };

        //examples[0].getEffect().debug = true;

        JTabbedPane tabbedPane = new JTabbedPane();

        tabbedPane.addTab("About", about);

        for(Example example : examples) {
            ExamplePanel examplePanel = new ExamplePanel(example);
            tabbedPane.addTab(example.getName(), examplePanel);
        }

        JFrame frame = new JFrame("PolySurfaces");
        frame.setSize(1000, 800);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(tabbedPane);
        //frame.pack();
        frame.setVisible(true);
    }
}
