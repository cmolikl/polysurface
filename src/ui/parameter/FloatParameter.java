/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ui.parameter;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author cmolikl
 */
public class FloatParameter extends Parameter { //implements Interpolable<GlslProgramFloatParameter> {
    float value;
    
    public FloatParameter(String name, float value) {
        super(name);
        this.value = value;
    }

    public float getValue() {
        return value;
    }
    
     public void setValue(float value) {
        if(this.value != value) {
            this.value = value;
            ChangeEvent e = new ChangeEvent(this);
            for(ChangeListener l : listeners) {
                l.stateChanged(e);
            }
        }
     }
    
    public boolean parseValue(String string) {
       try {
           setValue(Float.parseFloat(string));
       }
       catch(Exception e) {
           return false;
       }
       return true;
    }
    
    public String toString() {
        return "" + value;
    }

//    @Override
//    public void interpolate(GlslProgramFloatParameter value1, GlslProgramFloatParameter value2, float t) {
//        if(t <= 0) {
//            this.value = value1.getValue();
//        }
//        else if (t >= 1){
//            this.value = value2.getValue();
//        }
//        else {
//            this.value = (1f - t)*value1.getValue() + t*value2.getValue();
//        }
//    }
}
