package ui.parameter;

import javax.swing.event.ChangeListener;
import java.util.HashSet;

public abstract class Parameter {
    protected HashSet<ChangeListener> listeners = new HashSet<>();
    public String name;
    
    protected Parameter(String name) {
        this.name = name;
    }

    public void addChangeListener(ChangeListener l) {
        listeners.add(l);
    }
    
    public abstract boolean parseValue(String s);
    
    @Override
    public abstract String toString();

}
