/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui.control;

import ui.parameter.Parameter;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author cmolikl
 */
public class ParameterLabel extends JLabel {
    private class ParamChangeListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            Parameter p = (Parameter) e.getSource();
            setText(p.name + " = " + p.toString());
        }
    }

    public ParameterLabel(Parameter p) {
        super(p.name + " = " + p);
        p.addChangeListener(new ParamChangeListener());
    }
}
