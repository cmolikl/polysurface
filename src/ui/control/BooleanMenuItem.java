/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ui.control;

import ui.parameter.IntParameter;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author cmolikl
 */
public class BooleanMenuItem extends JCheckBoxMenuItem {

    private class ParamChangeListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            IntParameter p = (IntParameter) e.getSource();
            if(p.getValue() == 0 ) {
                if(isSelected()) setSelected(false);
            }
            else {
                if(!isSelected()) setSelected(true);
            }
        }
    }

    public BooleanMenuItem(String label, IntParameter param) {
        super(label);
        addActionListener(new BooleanActionListener(param));
        if(param.getValue() == 0) {
            setSelected(false);
        }
        else {
            setSelected(true);
        }
        param.addChangeListener(new ParamChangeListener());
    }

}
