/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui.control;

import ui.parameter.IntParameter;

import javax.swing.*;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Jakub
 */
public class OneOfNGroup {

    private final ButtonGroup group;

    public OneOfNGroup(JComponent component, int num, String[] names) {
        group = new ButtonGroup();
        int value = 1;
        for (int i = 0; i < num; i++) {
            final IntParameter ip = new IntParameter("parameter " + i, value);
            OneOfNMenuItem rbMenuItem = new OneOfNMenuItem(names[i], group, ip);
            group.add(rbMenuItem);
            component.add(rbMenuItem);
            value = 0;
        }
    }

    public OneOfNGroup(JComponent component, int num, ChangeListener changeListener, String[] names) {
        group = new ButtonGroup();
        int value = 1;
        for (int i = 0; i < num; i++) {
            final IntParameter ip = new IntParameter(names[i], value);
            OneOfNMenuItem rbMenuItem = new OneOfNMenuItem(names[i], group, ip);
            rbMenuItem.addChangeListener(changeListener);
            group.add(rbMenuItem);
            component.add(rbMenuItem);
            value = 0;
        }
    }

    public ButtonGroup getGroup() {
        return group;
    }

}
