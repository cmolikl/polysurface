/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ui.control;

import ui.parameter.IntParameter;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author cmolikl
 */
public class IntSlider extends JSlider {

    private class ParamChangeListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            IntParameter p = (IntParameter) e.getSource();
            int value = p.getValue();
            if(getValue() != value) {
                setValue(p.getValue());
            }
        }
    }

    private int defaultMajorTickSpacing = 25;
    public float min;
    public float max;
    
    public IntSlider(IntParameter param, int orientation, int min, int max) {
        super(orientation, min, max, param.getValue());
        setMajorTickSpacing(1);
        setMinorTickSpacing(1);
        setPaintTicks(true);
        setPaintLabels(true);

        //this.min = min;
        //this.max = max;
        addChangeListener(new IntSliderListener(param));
        param.addChangeListener(new ParamChangeListener());
    }
}
