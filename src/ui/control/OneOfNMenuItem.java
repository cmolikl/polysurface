/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui.control;

import ui.parameter.IntParameter;

import javax.swing.*;

/**
 *
 * @author Jakub
 */
public class OneOfNMenuItem extends JRadioButtonMenuItem {

    private IntParameter param;

    public OneOfNMenuItem(String label, ButtonGroup group, IntParameter param) {
        super(label);
        this.param = param;
        if (param.getValue() == 0) {
            setSelected(false);
        } else {
            setSelected(true);
        }
        addActionListener(new OneOfNActionListener(param, group));
    }

    public IntParameter getParam() {
        return param;
    }
}
